#ifndef CAMERA_COMPONENT_H
#define CAMERA_COMPONENT_H

#include "gameComponent.h"
#include "transform.h"

namespace dr
{

	class CameraComponent : public dr::GameComponent
	{
	private:
		dr::Transform m_cameraTransform;
		dr::Matrix4f m_projectionMatrix;
	public:
		CameraComponent(void);
		~CameraComponent(void);

		dr::Transform getCameraTransform(void) const;
		dr::Matrix4f getProjectionMatrix(void) const;
		dr::Matrix4f getViewProjectionMatrix(void) const;

		void setCameraTransform(const dr::Transform& transform);
		void setProjectionMatrix(const dr::Matrix4f& projection);
	};
}

#endif