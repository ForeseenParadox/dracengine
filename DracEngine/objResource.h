#ifndef OBJ_RESOURCE_H
#define OBJ_RESOURCE_H

#include "singleResource.h"
#include <vector>
#include "vec2.h"
#include "vec3.h"
#include "color.h"
#include "objIndex.h"

namespace dr
{

	class OBJResource : public dr::SingleResource
	{
	private:
		std::vector<dr::Vec3f> m_vertices;
		std::vector<dr::Vec2f> m_texCoords;
		std::vector<dr::Vec3f> m_normals;
		std::vector<dr::OBJIndex> m_indicies;
		bool m_isTriangulated;

		bool m_isUsingVertices;
		bool m_isUsingTexCoords;
		bool m_isUsingNormals;
	public:

		OBJResource(const std::string& path);
		~OBJResource(void);

		std::vector<dr::Vec3f> getVertices(void) const;
		std::vector<dr::Vec2f> getTexCoords(void) const;
		std::vector<dr::Vec3f> getNormals(void) const;
		std::vector<OBJIndex> getIndicies(void) const;
		dr::Vec3f getVertex(int index) const;
		dr::Vec2f getTexCoord(int index) const;
		dr::Vec3f getNormal(int index) const;
		dr::OBJIndex getIndex(int index) const;
		bool isTriangulated(void) const;
		bool isUsingVertices(void) const;
		bool isUsingTexCoords(void) const;
		bool isUsingNormals(void) const;

		void load(void);
		void unload(void);
	};
}

#endif