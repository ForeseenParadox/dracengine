#ifndef SINGLE_RESOURCE_H
#define SINGLE_RESOURCE_H

#include "resource.h"

namespace dr
{
	class SingleResource : public Resource
	{
	private:
		std::string m_location;
	public:
		SingleResource(const std::string& loc);
		virtual ~SingleResource(void);

		std::string& getLocation(void);

		virtual void load(void) = 0;
		virtual void unload(void) = 0;
	};
}

#endif