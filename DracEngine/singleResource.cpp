#include "singleResource.h"

dr::SingleResource::SingleResource(const std::string& loc)
{
	m_location = loc;
}

dr::SingleResource::~SingleResource(void)
{

}

std::string& dr::SingleResource::getLocation(void)
{
	return m_location;
}