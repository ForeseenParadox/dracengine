#ifndef VEC_2_H
#define VEC_2_H

namespace dr
{
	class Vec2f
	{
	private:
		float m_x;
		float m_y;
	public:

		Vec2f(void);
		Vec2f(float x, float y);
		Vec2f(const dr::Vec2f& other);
		Vec2f(dr::Vec2f&& other);
		~Vec2f(void);

		float getX(void) const;
		float getY(void) const;

		void setX(float x);
		void setY(float y);

		float length(void) const;
		float distance(const dr::Vec2f& other) const;
		float dot(const dr::Vec2f& other) const;

		void initZero(void);

		Vec2f* mutateAdd(const dr::Vec2f& other);
		Vec2f* mutateAdd(float x, float y);
		Vec2f* mutateSubtract(const dr::Vec2f& other);
		Vec2f* mutateSubtract(float x, float y);
		Vec2f* mutateScale(float scaler);
		Vec2f* mutateScale(float scaleX, float scaleY);
		Vec2f* mutateScale(const dr::Vec2f& other);
		Vec2f* mutateNormalize(void);

		Vec2f add(const dr::Vec2f& other) const;
		Vec2f add(float x, float y) const;
		Vec2f subtract(const dr::Vec2f& other) const;
		Vec2f subtract(float x, float y) const;
		Vec2f scale(float scaler) const;
		Vec2f scale(float scaleX, float scaleY) const;
		Vec2f scale(const dr::Vec2f& other) const;
		Vec2f normalize(void);

		Vec2f operator+(const Vec2f& other);
		Vec2f operator-(const Vec2f& other);
		Vec2f operator*(const Vec2f& other);
		Vec2f operator*(float scaler);

		Vec2f operator+=(const Vec2f& other);
		Vec2f operator-=(const Vec2f& other);
		Vec2f operator*=(const Vec2f& other);
		Vec2f operator*=(float scaler);

		bool operator==(const dr::Vec2f& other) const;

	};
}

#endif
