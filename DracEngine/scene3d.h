#ifndef SCENE_3_D
#define SCENE_3_D

namespace dr
{

	class GameObject;

	class Scene3D
	{

	private:
		dr::GameObject* m_rootObject;
	public:
		Scene3D(void);
		Scene3D(dr::GameObject* rootObject);
		~Scene3D(void);

		dr::GameObject* getRootObject(void);
		void setRootObject(dr::GameObject* rootObject);
	};
}

#endif