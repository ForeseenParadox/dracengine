#ifndef TRANSFORM_H
#define TRANSFORM_H

#include "mat4.h"

namespace dr
{

	class Transform
	{
	private:
		dr::Matrix4f m_translation;
		dr::Matrix4f m_rotation;
		dr::Matrix4f m_scale;
	public:
		Transform(void);
		Transform(const dr::Transform& other);
		~Transform(void);

		dr::Matrix4f getTranslation(void) const;
		dr::Matrix4f getRotation(void) const;
		dr::Matrix4f getScale(void) const;
		dr::Matrix4f getTransform(void) const;

		void setTranslation(float tx, float ty, float tz);
		void setRotation(float rx, float ry, float rz);
		void setScale(float sx, float sy, float sz);

		void setTranslation(dr::Matrix4f translation);
		void setRotation(dr::Matrix4f rotation);
		void setScale(dr::Matrix4f scale);
	};

}

#endif