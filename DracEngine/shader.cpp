#include "shader.h"
#include <iostream>
#include "GL\glew.h"

std::unordered_map<GLuint, std::unordered_map<std::string, GLint>> dr::ShaderProgram::s_uniformLocationMemo;

dr::ShaderProgram::ShaderProgram(void):
m_programHandle(glCreateProgram()),
m_vertexHandle(-1),
m_fragmentHandle(-1)
{
	s_uniformLocationMemo[m_programHandle] = std::unordered_map <std::string, GLint>();
}

dr::ShaderProgram::ShaderProgram(const dr::VertexAttrib attribs[], int attribCount):
m_programHandle(glCreateProgram()),
m_vertexHandle(-1),
m_fragmentHandle(-1)
{
	s_uniformLocationMemo[m_programHandle] = std::unordered_map <std::string, GLint>();
	bindAttribs(attribs, attribCount);
}

dr::ShaderProgram::ShaderProgram(const dr::ShaderResource& resource, const dr::VertexAttrib attribs[], int attribCount):
m_programHandle(glCreateProgram()),
m_vertexHandle(-1),
m_fragmentHandle(-1)
{
	s_uniformLocationMemo[m_programHandle] = std::unordered_map <std::string, GLint>();
	compileAndLink(resource.getVertexShaderSource().getLoadedText(), resource.getFragmentShaderSource().getLoadedText());
}

dr::ShaderProgram::ShaderProgram(const std::string& vertexSource, const std::string& fragmentSource, const dr::VertexAttrib attribs[], int attribCount):
m_programHandle(glCreateProgram()),
m_vertexHandle(-1),
m_fragmentHandle(-1)
{
	s_uniformLocationMemo[m_programHandle] = std::unordered_map <std::string, GLint>();
	bindAttribs(attribs, attribCount);
	compileAndLink(vertexSource, fragmentSource);
}

dr::ShaderProgram::~ShaderProgram(void)
{
	glDeleteProgram(m_programHandle);
}

GLuint dr::ShaderProgram::getProgramHandle(void) const
{
	return m_programHandle;
}

GLuint dr::ShaderProgram::getVertexHandle(void) const
{
	return m_vertexHandle;
}

GLuint dr::ShaderProgram::getFragmentHandle(void) const
{
	return m_fragmentHandle;
}

bool dr::ShaderProgram::isVertexShaderCreated(void) const
{
	return m_vertexHandle > -1;
}

bool dr::ShaderProgram::isFragmentShaderCreated(void) const
{
	return m_fragmentHandle > -1;
}


void dr::ShaderProgram::compileAndLink(const dr::ShaderResource& resource)
{
	compileAndLink(resource.getVertexShaderSource().getLoadedText(), resource.getFragmentShaderSource().getLoadedText());
}

void dr::ShaderProgram::compileAndLink(const std::string& vertexSource, const std::string& fragmentSource)
{
	compileVertexShader(vertexSource);
	compileFragmentShader(fragmentSource);
	linkProgram();
}

void dr::ShaderProgram::compileVertexShader(const std::string& vertexSource)
{
	m_vertexHandle = createShader(vertexSource, GL_VERTEX_SHADER);
}

void dr::ShaderProgram::compileFragmentShader(const std::string& fragmentSource)
{
	m_fragmentHandle = createShader(fragmentSource, GL_FRAGMENT_SHADER);
}

void dr::ShaderProgram::linkProgram(void)
{
	glAttachShader(m_programHandle, m_vertexHandle);
	glAttachShader(m_programHandle, m_fragmentHandle);

	glLinkProgram(m_programHandle);

	int result = 0;
	glGetProgramiv(m_programHandle, GL_LINK_STATUS, &result);

	if (result != GL_TRUE)
	{
		int infoLogLength = 0;
		
		glGetProgramiv(m_programHandle, GL_INFO_LOG_LENGTH, &infoLogLength);
	
		char* info = new char[infoLogLength];
		glGetProgramInfoLog(m_programHandle, infoLogLength, NULL, info);

		std::cout << std::string(info) << std::endl;
	}
	
}

int dr::ShaderProgram::createShader(const std::string& source, int type)
{
	int handle = glCreateShader(type);

	const GLchar* src = source.c_str();

	GLint* countArray = new GLint[]{static_cast<GLint>(strlen(source.c_str()))};
	glShaderSource(handle, 1, &src, countArray);

	glCompileShader(handle);

	int result = 0;
	glGetShaderiv(handle, GL_COMPILE_STATUS, &result);

	if (result != GL_TRUE)
	{	
		int infoLogLength = 0;
		glGetShaderiv(handle, GL_INFO_LOG_LENGTH, &infoLogLength);
		char* info = new char[infoLogLength];
		glGetShaderInfoLog(handle, infoLogLength, NULL, info);
		std::cout << std::string(info) << std::endl;
	}

	return handle;
}

void dr::ShaderProgram::bindAttribs(const dr::VertexAttrib attribs[], int attribCount)
{
	if (m_programHandle > 0)
	{
		for (int i = 0; i < attribCount; i++)
			glBindAttribLocation(m_programHandle, attribs[i].getIndex(), attribs[i].getName().c_str());
	}
	else
	{
		std::string msg = "Can not bind vertex attribs when program has not yet been created.";
		std::cerr << msg << std::endl;
		throw std::runtime_error(msg);
	}
}

void dr::ShaderProgram::bindProgram(void) const
{
	glUseProgram(m_programHandle);
}

int dr::ShaderProgram::getUniformLocation(const std::string& name) const
{
	std::unordered_map <std::string, GLint> map = s_uniformLocationMemo[m_programHandle];
	if (map.find(name) == map.end())
	{
		map[name] = glGetUniformLocation(m_programHandle, name.c_str());
	}
	return map.find(name)->second;
}

void dr::ShaderProgram::setUniformInt(const std::string& name, int i) const
{
	glUniform1i(getUniformLocation(name), i);
}

void dr::ShaderProgram::setUniformFloat(const std::string& name, float f) const
{
	glUniform1f(getUniformLocation(name), f);
}

void dr::ShaderProgram::setUniformVector2(const std::string& name, const dr::Vec2f& vector) const
{
	glUniform2f(getUniformLocation(name), vector.getX(), vector.getY());
}

void dr::ShaderProgram::setUniformVector3(const std::string& name, const dr::Vec3f& vector) const
{
	glUniform3f(getUniformLocation(name), vector.getX(), vector.getY(), vector.getZ());
}

void dr::ShaderProgram::setUniformColor3(const std::string& name, const dr::Color& color) const
{
	glUniform3f(getUniformLocation(name), color.getRed(), color.getGreen(), color.getBlue());
}

void dr::ShaderProgram::setUniformColor4(const std::string& name, const dr::Color& color) const
{
	glUniform4f(getUniformLocation(name), color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
}

void dr::ShaderProgram::setUniformMatrix4f(const std::string& name, const dr::Matrix4f& mat) const
{
	glUniformMatrix4fv(getUniformLocation(name), 1, true, mat.getData());
}