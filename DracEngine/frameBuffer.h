#ifndef FRAME_BUFFER_H
#define FRAME_BUFFER_H

#include "GL\glew.h"
#include "texture.h"

namespace dr
{
	class FrameBuffer
	{
	private:
		static const int S_DEFAULT_DRAWBUFFER_COUNT;
		static GLenum S_DEFAULT_DRAWBUFFERS[];
		
		GLuint m_handle;
		dr::Texture m_defaultColorAttachment;
		const dr::Texture* m_colorAttachment;

		void initDefaultColorAttachment(void);
	public:
		FrameBuffer(void);
		FrameBuffer(bool colorBuffer, bool depthBuffer, bool stencilBuffer);
		~FrameBuffer(void);

		GLuint getHandle(void) const;
		const dr::Texture* getColorAttachment(void) const;

		void setDrawBuffers(GLenum drawBuffers[], int count);
		void attachTexture(const dr::Texture* texture, GLenum attachment);
		void checkComplete(void);

		void bind(void);
		static void bindDefaultFrameBuffer();
	};
}

#endif

