#include "lights2d.h"

dr::Light::Light(void):
m_intensity(1),
m_color()
{
}

dr::Light::Light(const dr::Light& other):
m_intensity(other.m_intensity),
m_color(other.m_color)
{
}

dr::Light::Light(float intensity, const dr::Color& color):
m_intensity(intensity),
m_color(color)
{
}

dr::AmbientLight::AmbientLight(void):
m_base()
{
}

dr::AmbientLight::AmbientLight(const dr::AmbientLight& other) :
m_base(other.m_base)
{
}

dr::AmbientLight::AmbientLight(float intensity, const dr::Color& color):
m_base(intensity, color)
{
}

dr::PointLight::PointLight(void):
m_base(),
m_position(),
m_attenuation()
{
}

dr::PointLight::PointLight(const dr::PointLight& other):
m_base(other.m_base),
m_position(other.m_position),
m_attenuation(other.m_attenuation)
{
}

dr::PointLight::PointLight(float intensity, const dr::Color& color, const dr::Vec2f& position, const dr::Vec3f attenuation):
m_base(intensity, color),
m_position(position),
m_attenuation(attenuation)
{
}