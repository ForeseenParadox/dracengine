#ifndef PRIMITIVE_H
#define PRIMITIVE_H

namespace dr
{
	enum Primitive : unsigned int
	{
		LINE = 0, TRIANGLE = 1, QUAD = 2
	};
}

#endif