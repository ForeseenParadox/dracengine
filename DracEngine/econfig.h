#ifndef CONFIG_H
#define CONFIG_H

#include <string>
#include "windowInfo.h"

namespace dr
{
	class BaseGame;

	class EngineConfig
	{
	private:
		int m_maxUps;
		int m_maxFps;
		dr::WindowInfo m_windowInfo;
	public:
		EngineConfig(void);
		EngineConfig(int maxUps, int maxFps, const dr::WindowInfo& windowInfo);
		EngineConfig(const dr::EngineConfig& other);
		~EngineConfig(void);

		int getMaxUps(void) const;
		int getMaxFps(void) const;
		dr::WindowInfo getWindowInfo(void) const;

		void setMaxUps(int maxUps);
		void setMaxFps(int maxFps);
		void setWindowInfo(const dr::WindowInfo& windowInfo);

		dr::EngineConfig& operator=(const dr::EngineConfig& other);
	};
}

#endif