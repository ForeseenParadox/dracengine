#ifndef LIGHTS_2D_H
#define LIGHTS_2D_H

#include "color.h"
#include "vec2.h"
#include "vec3.h"

namespace dr
{

	struct Light
	{
		float m_intensity;
		dr::Color m_color;

		Light(void);
		Light(const dr::Light& other);
		Light(float intensity, const dr::Color& color);
	};

	struct AmbientLight
	{
		dr::Light m_base;

		AmbientLight(void);
		AmbientLight(const dr::AmbientLight& other);
		AmbientLight(float intensity, const dr::Color& color);
	};

	struct PointLight
	{
		dr::Light m_base;
		dr::Vec2f m_position;
		dr::Vec3f m_attenuation;

		PointLight(void);
		PointLight(const dr::PointLight& other);
		PointLight(float intensity, const dr::Color& color, const dr::Vec2f& position, const dr::Vec3f attenuation);
	};

}

#endif