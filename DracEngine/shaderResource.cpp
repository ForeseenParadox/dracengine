#include "shaderResource.h"

dr::ShaderResource::ShaderResource(const std::string& vertexShaderPath, const std::string& fragmentShaderPath) :
m_vertexShaderSource(vertexShaderPath),
m_fragmentShaderSource(fragmentShaderPath),
CompositeResource(new Resource*[SHADER_RESOURCE_COUNT]{&m_vertexShaderSource, &m_fragmentShaderSource}, SHADER_RESOURCE_COUNT)
{

}

dr::ShaderResource::~ShaderResource(void)
{
}

dr::TextResource dr::ShaderResource::getVertexShaderSource() const
{
	return m_vertexShaderSource;
}

dr::TextResource dr::ShaderResource::getFragmentShaderSource() const
{
	return m_fragmentShaderSource;
}

void dr::ShaderResource::load(void)
{
	// for now, call super method
	// later add custom syntax to glsl shaders for the engine
	dr::CompositeResource::load();
}

void dr::ShaderResource::unload(void)
{
	dr::CompositeResource::load();
}