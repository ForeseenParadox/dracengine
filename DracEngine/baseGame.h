#ifndef BASE_GAME_H
#define BASE_GAME_H

namespace dr
{

	class Engine;

	class BaseGame
	{
	private:
		dr::Engine* m_engine;
	public:
		BaseGame(dr::Engine* engine);
		virtual ~BaseGame(void);

		dr::Engine* getEngine(void) const;

		virtual void load(void) = 0;
		virtual void resized(void);
		virtual void preUpdate(float delta);
		virtual void update(float delta) = 0;
		virtual void preRender(void);
		virtual void postLoad(void);
		virtual void render(void) = 0;
		virtual void dispose(void) = 0;
	};
}

#endif