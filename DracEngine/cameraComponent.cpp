#include "cameraComponent.h"
#include "dmath.h"
#include "engine.h"
#include "window.h"

dr::CameraComponent::CameraComponent(void)
{
	dr::Window* wnd = Engine::getInstance()->getWindow();
	m_projectionMatrix.initPerspectiveProjection(70.0f * (dr::PI / 180.0f), wnd->getWidth() / (float)wnd->getHeight(), 0.01, -1000);
}

dr::CameraComponent::~CameraComponent(void)
{

}

dr::Transform dr::CameraComponent::getCameraTransform(void) const
{
	return m_cameraTransform;
}

dr::Matrix4f dr::CameraComponent::getProjectionMatrix(void) const
{
	return m_projectionMatrix;
}

dr::Matrix4f dr::CameraComponent::getViewProjectionMatrix(void) const
{
	return m_cameraTransform.getTransform().multiply(m_projectionMatrix);
}

void dr::CameraComponent::setCameraTransform(const dr::Transform& transform)
{
	m_cameraTransform = transform;
}

void dr::CameraComponent::setProjectionMatrix(const dr::Matrix4f& projection)
{
	m_projectionMatrix = projection;
}