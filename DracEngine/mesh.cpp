#include "mesh.h"
#include "GL\glew.h"
#include <vector>
#include <iostream>
#include <cstdlib>
#include "vertex3.h"
#include "color.h"
#include "singleNode.h"

dr::Mesh::Mesh(void)
{
	m_attribCount = 4;
	m_attribs = new VertexAttrib[m_attribCount]{VertexAttrib(0, 3, "a_position"), VertexAttrib(1, 4, "a_color"), VertexAttrib(2, 2, "a_texCoord"), VertexAttrib(3, 3, "a_normal")};

	m_vao = new VertexArray;
	m_vbo = nullptr;
	m_ibo = nullptr;
}

dr::Mesh::Mesh(int vboSize)
{
	m_attribCount = 4;
	m_attribs = new VertexAttrib[m_attribCount]{VertexAttrib(0, 3, "a_position"), VertexAttrib(1, 4, "a_color"), VertexAttrib(2, 2, "a_texCoord"), VertexAttrib(3, 3, "a_normal")};

	m_vao = new VertexArray;
	m_vbo = new VertexBuffer(vboSize);
	m_ibo = nullptr;
}

dr::Mesh::Mesh(int vboSize, int indexCount)
{
	m_attribCount = 4;
	m_attribs = new VertexAttrib[m_attribCount]{VertexAttrib(0, 3, "a_position"), VertexAttrib(1, 4, "a_color"), VertexAttrib(2, 2, "a_texCoord"), VertexAttrib(3, 3, "a_normal")};

	m_vao = new VertexArray;
	m_vbo = new VertexBuffer(vboSize);
	m_ibo = new IndexBuffer(indexCount);
}

dr::Mesh::Mesh(dr::OBJResource* objResource)
{

	// attribs
	m_attribCount = 4;
	m_attribs = new VertexAttrib[m_attribCount]{VertexAttrib(0, 3, "a_position"), VertexAttrib(1, 4, "a_color"), VertexAttrib(2, 2, "a_texCoord"), VertexAttrib(3, 3, "a_normal")};	
	
	if (objResource != nullptr)
	{

		std::vector<dr::Vertex3> glVertices;
		std::vector<unsigned int> glIndices;

		int vertexCount = objResource->getVertices().size();
		int referencedVertexCount = objResource->getIndicies().size();
		int currentGlIndex = 0;

		int face = 0;
		int r = 0;
		int g = 0;
		int b = 0;

		for (int i = 0; i < referencedVertexCount; i++)
		{
			dr::OBJIndex index = objResource->getIndex(i);
			dr::Vertex3 vert;

			if (face++ % 3 == 0)
			{
				r = (rand() % 256);
				g = (rand() % 256);
				b = (rand() % 256);
			}

			vert.setColor(dr::Color(r / 255.0f, g / 255.0f, b / 255.0f, 1.0f));
			// set attributes

			if (objResource->isUsingVertices())
				vert.setPosition(objResource->getVertex(index.getVertexIndex()));
			if (objResource->isUsingTexCoords())
				vert.setTexCoord(objResource->getTexCoord(index.getTexCoordIndex()));
			if (objResource->isUsingNormals())
				vert.setNormal(objResource->getNormal(index.getNormalIndex()));

			// mesh indexing
			/*
			int existingIndex = -1;
			for (int j = 0; j < glVertices.size(); j++)
				if (glVertices.at(j) == vert)
					existingIndex = j;
				

			if (existingIndex > -1)
				glIndices.push_back(existingIndex);
			else
			{
				glVertices.push_back(vert);
				glIndices.push_back(currentGlIndex++);
			}*/

			glVertices.push_back(vert);
			glIndices.push_back(currentGlIndex++);
		}

		m_vao = new VertexArray;
		m_vbo = new VertexBuffer(glVertices.size() * (3 + 4 + 2 + 3));
		m_ibo = new IndexBuffer(glIndices.size());

		// add vertex data to vbo
		for (std::vector<dr::Vertex3>::iterator itr = glVertices.begin(); itr != glVertices.end(); itr++, m_vertexCount++)
			m_vbo->pushData(*itr);

		// add index data to ibo
		for (std::vector<unsigned int>::iterator itr = glIndices.begin(); itr != glIndices.end(); itr++)
			m_ibo->pushIndex(*itr);


		m_ibo->flushData(GL_STATIC_DRAW);
		m_vao->bindBuffer(m_vbo, m_attribs, m_attribCount);
	}
}

dr::Mesh::~Mesh(void)
{
	if (m_vao != nullptr)
		delete m_vao;
	if (m_vbo != nullptr)
		delete m_vbo;
	if (m_ibo != nullptr)
		delete m_ibo;
}

dr::VertexArray* dr::Mesh::getVertexArray(void)
{
	return m_vao;
}

dr::VertexBuffer* dr::Mesh::getVertexBuffer(void)
{
	return m_vbo;
}

dr::IndexBuffer* dr::Mesh::getIndexBuffer(void)
{
	return m_ibo;
}

int dr::Mesh::getAttribCount(void) const
{
	return m_attribCount;
}

dr::VertexAttrib* dr::Mesh::getVertexAttribs(void)
{
	return m_attribs;
}

int dr::Mesh::getVertexCount(void) const
{
	return m_vertexCount;
}

void dr::Mesh::setVertexArray(dr::VertexArray* vao)
{
	m_vao = vao;
}

void dr::Mesh::setVertexBuffer(dr::VertexBuffer* vbo)
{
	m_vbo = vbo;
}

void dr::Mesh::setIndexBuffer(dr::IndexBuffer* ibo)
{
	m_ibo = ibo;
}

void dr::Mesh::setVertexAttribs(dr::VertexAttrib* attribs, int attribCount)
{
	m_attribCount = attribCount;
	m_attribs = attribs;
}

void dr::Mesh::render(void)
{
	if (m_vao != nullptr && m_vbo != nullptr)
		m_vao->render(GL_TRIANGLES, m_vbo->getPosition());
}