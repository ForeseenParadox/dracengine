#ifndef SCRIPT_RESOURCE_H
#define SCRIPT_RESOURCE_H

#include "textResource.h"
#include <string>

namespace dr
{

	class ScriptResource : public dr::TextResource
	{
	private:

	public:
		ScriptResource(const std::string& path);
		~ScriptResource(void);

		void load(void);
		void unload(void);
	};

}

#endif