#include "vertexBuffer.h"
#include <iostream>

dr::VertexBuffer::VertexBuffer(int stackSize) :
m_handle(0),
m_stackSize(stackSize),
m_position(0),
m_dataStack(new float[stackSize]),
m_drawMode(GL_STATIC_DRAW)
{
	initBuffer();
}

dr::VertexBuffer::VertexBuffer(GLenum drawMode, int stackSize) :
m_handle(0),
m_stackSize(stackSize),
m_position(0),
m_dataStack(new float[stackSize]),
m_drawMode(drawMode)
{
	initBuffer();
}

dr::VertexBuffer::~VertexBuffer(void)
{
	glDeleteBuffers(1, &m_handle);
	delete m_dataStack;
}

void dr::VertexBuffer::initBuffer()
{
	glGenBuffers(1, &m_handle);

	// inital allocation
	bind();
	glBufferData(GL_ARRAY_BUFFER, m_stackSize * sizeof(float), NULL, m_drawMode);
}

void dr::VertexBuffer::bind(void) const
{
	glBindBuffer(GL_ARRAY_BUFFER, m_handle);
}

GLuint dr::VertexBuffer::getHandle(void) const
{
	return m_handle;
}

unsigned int dr::VertexBuffer::getPosition(void) const
{
	return m_position;
}

unsigned int dr::VertexBuffer::getStackSize(void) const
{
	return m_stackSize;
}

GLenum dr::VertexBuffer::getDrawMode(void) const
{
	return m_drawMode;
}

void dr::VertexBuffer::setDrawMode(GLenum drawMode)
{
	m_drawMode = drawMode;
}

dr::VertexBuffer& dr::VertexBuffer::pushData(float data)
{
	if (m_position + 1 > m_stackSize)
		throw std::runtime_error("Stack overflow error on VBO with handle " + m_handle + '.');
	else
		m_dataStack[m_position++] = data;
	
	return *this;
}

dr::VertexBuffer& dr::VertexBuffer::pushData(dr::Vertex3& vertex)
{
	if (m_position + dr::VERTEX_3_SIZE > m_stackSize)
		throw std::runtime_error("Stack overflow error on VBO with handle " + m_handle + '.');
	else
	{
		m_dataStack[m_position++] = vertex.getPosition().getX();
		m_dataStack[m_position++] = vertex.getPosition().getY();
		m_dataStack[m_position++] = vertex.getPosition().getZ();

		m_dataStack[m_position++] = vertex.getColor().getRed();
		m_dataStack[m_position++] = vertex.getColor().getGreen();
		m_dataStack[m_position++] = vertex.getColor().getBlue();
		m_dataStack[m_position++] = vertex.getColor().getAlpha();

		m_dataStack[m_position++] = vertex.getTexCoord().getX();
		m_dataStack[m_position++] = vertex.getTexCoord().getY();

		m_dataStack[m_position++] = vertex.getNormal().getX();
		m_dataStack[m_position++] = vertex.getNormal().getY();
		m_dataStack[m_position++] = vertex.getNormal().getZ();
	}

	return *this;
}

dr::VertexBuffer& dr::VertexBuffer::pushData(float data[], int count)
{
	// move data to stack
	for (int i = 0; i < count; i++)
	{
		if (m_position + 1 >= m_stackSize)
			throw std::runtime_error("Stack overflow error on VBO with handle " + m_handle + '.');
		else
			m_dataStack[m_position++] = data[i];
	}

	return *this;
}

dr::VertexBuffer& dr::VertexBuffer::popData(void)
{
	if (m_position <= 0)
	{
		throw std::runtime_error("Empty stack exception on VBO with handle " + m_handle + '.');
	} else
	{
		m_position--;

		return *this;
	}
}

void dr::VertexBuffer::clearData(void)
{
	m_position = 0;
}

void dr::VertexBuffer::flushData()
{	
	bind();
	glBufferSubData(GL_ARRAY_BUFFER, 0, m_position * sizeof(float), m_dataStack);
}

void dr::VertexBuffer::unbindVBO(void)
{
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}