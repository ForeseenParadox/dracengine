#include "singleNode.h"

template<class T>
dr::SingleNode<T>::SingleNode(void):
m_value(nullptr),
m_next(nullptr)
{
}

template<class T>
dr::SingleNode<T>::SingleNode(T* value, SingleNode<T>* next) :
m_value(value),
m_next(next)
{
}

template<class T>
dr::SingleNode<T>::~SingleNode(void)
{

}

template<class T>
T* dr::SingleNode<T>::getValue(void)
{
	return m_value;
}

template<class T>
dr::SingleNode<T>* dr::SingleNode<T>::getNext(void)
{
	return m_next;
}

template<class T>
void dr::SingleNode<T>::setValue(T* value)
{
	m_value = value;
}

template<class T>
void dr::SingleNode<T>::setNext(SingleNode<T>* next)
{
	m_next = next;
}