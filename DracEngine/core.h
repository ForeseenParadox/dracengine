#ifndef CORE_H
#define CORE_H

namespace dr
{
	class Subsystem;

	class Core
	{

	private:

		int m_subsystemCount;
		int m_updatableSubsystemCount;
		int m_renderableSubsystemCount;

		dr::Subsystem** m_initializeQueue;
		dr::Subsystem** m_updateQueue;
		dr::Subsystem** m_renderQueue;

	public:
		Core(int subsystemCount, int updatableSubsystemCount, int renderableSubsystemCount);
		~Core(void);

		int getInitializeCount(void) const;
		int getUpdateCount(void) const;
		int getRenderCount(void) const;

		dr::Subsystem** getInitializeQueue(void) const;
		dr::Subsystem** getUpdateQueue(void) const;
		dr::Subsystem** getRenderQueue(void) const;

		void insertSubsystem(int priority, dr::Subsystem* subsystem);
		void insertUpdatableSubsystem(int priority, dr::Subsystem* subsystem);
		void insertRenderableSubsystem(int priority, dr::Subsystem* subsystem);

		void initSubsystems(void);
		void updateSubsystems(float delta, bool preUpdate);
		void renderSubsystems(bool preRender);
		void disposeSubsystems(void);
	};
}

#endif