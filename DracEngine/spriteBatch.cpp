#include <iostream>
#include "spriteBatch.h"
#include "window.h"
#include "engine.h"

const dr::VertexAttrib dr::SpriteBatch::ATTRIBS[ATTRIB_COUNT] = { dr::VertexAttrib(0, 2, dr::POSITION_ATTRIB_NAME), dr::VertexAttrib(1, 4, dr::COLOR_ATTRIB_NAME), dr::VertexAttrib(2, 2, dr::TEXCOORD_ATTRIB_NAME) };

dr::SpriteBatch::SpriteBatch(void):
m_maxDraws(DEFAULT_MAX_DRAWS),
m_vbo(DEFAULT_MAX_DRAWS * 4 * 8),
m_customShader(nullptr),
m_began(false)
{
	initSpriteBatch();
}

dr::SpriteBatch::SpriteBatch(int maxDraws):
m_maxDraws(maxDraws),
m_vbo(maxDraws * 4 * 8),
m_customShader(nullptr),
m_began(false)
{
	initSpriteBatch();
}

dr::SpriteBatch::SpriteBatch(dr::ShaderProgram* customShader) :
m_maxDraws(DEFAULT_MAX_DRAWS),
m_vbo(DEFAULT_MAX_DRAWS * 4 * 8),
m_customShader(customShader),
m_began(false)
{
	initSpriteBatch();
}

dr::SpriteBatch::SpriteBatch(dr::ShaderProgram* customShader, int maxDraws) :
m_maxDraws(maxDraws),
m_vbo(maxDraws * 4 * 8),
m_customShader(customShader),
m_began(false)
{
	initSpriteBatch();
}

dr::SpriteBatch::SpriteBatch(const dr::SpriteBatch& other) :
m_maxDraws(other.m_maxDraws),
m_vbo(other.m_vbo.getStackSize()),
m_customShader(other.getCustomShader()),
m_began(false)
{

}

void dr::SpriteBatch::initSpriteBatch()
{
	// create first time view projection matrix
	dr::Window* wnd = Engine::getInstance()->getWindow();
	m_viewProjection.initOrthographicProjection(0, wnd->getWidth(), 0, wnd->getHeight(), -1, 1);

	// create default shaders
	std::string vertexSource =
		std::string("#version 110\n") +
		std::string("uniform mat4 u_viewProjection;\n") +
		std::string("attribute vec2 a_position;\n") +
		std::string("attribute vec4 a_color;\n") +
		std::string("attribute vec2 a_texCoord;\n") +
		std::string("varying vec4 v_color;\n") +
		std::string("varying vec2 v_texCoord;\n") +
		std::string("void main()\n") +
		std::string("{\n") +
		std::string("	gl_Position = u_viewProjection * vec4(a_position, 0.0, 1.0);\n") +
		std::string("	v_color = a_color;\n") +
		std::string("	v_texCoord = a_texCoord;\n") +
		std::string("}\n");

	std::string fragmentSource =
		std::string("#version 110\n") +
		std::string("uniform sampler2D u_sampler;\n") +
		std::string("varying vec4 v_color;\n") +
		std::string("varying vec2 v_texCoord;\n") +
		std::string("void main()\n") +
		std::string("{\n") +
		std::string("	gl_FragColor = v_color * texture2D(u_sampler, v_texCoord);\n") +
		std::string("}\n");

	m_shader.bindAttribs(ATTRIBS, ATTRIB_COUNT);
	m_shader.compileAndLink(vertexSource, fragmentSource);

	// bind vertex buffer and attributes to vertex array
	m_vao.bindBuffer(&m_vbo, ATTRIBS, ATTRIB_COUNT);
}

dr::SpriteBatch::~SpriteBatch(void)
{

}

bool dr::SpriteBatch::hasBegan(void) const
{
	return m_began;
}

int dr::SpriteBatch::getMaxDraws(void) const
{
	return m_maxDraws;
}

int dr::SpriteBatch::getCurrentDraws(void) const
{
	return m_draws;
}

const dr::Texture* dr::SpriteBatch::getCurrentTexture(void) const
{
	return m_currentTexture;
}

dr::Color& dr::SpriteBatch::getTint(void)
{
	return m_tint;
}

const dr::ShaderProgram& dr::SpriteBatch::getShader(void) const
{
	return m_shader;
}

const dr::ShaderProgram* dr::SpriteBatch::getCustomShader(void) const
{
	return m_customShader;
}

void dr::SpriteBatch::setTint(const dr::Color& tint)
{
	m_tint = tint;
}

void dr::SpriteBatch::setCustomShader(const dr::ShaderProgram* customShader)
{
	m_customShader = customShader;
}

void dr::SpriteBatch::render(const dr::TextureRegion& texture, float x, float y)
{
	render(texture.getTexture(), x, y, texture.getNormalizedX(), texture.getNormalizedY(), texture.getNormalizedWidth(), texture.getNormalizedHeight(), 0, 0, 0, 1, 1);
}

void dr::SpriteBatch::render(const dr::TextureRegion& texture, float x, float y, float originX, float originY, float rotation)
{
	render(texture.getTexture(), x, y, texture.getNormalizedX(), texture.getNormalizedY(), texture.getNormalizedWidth(), texture.getNormalizedHeight(), originX, originY, rotation, 1, 1);
}

void dr::SpriteBatch::render(const dr::TextureRegion& texture, float x, float y, float originX, float originY, float rotation, float scaleX, float scaleY)
{
	render(texture.getTexture(), x, y, texture.getNormalizedX(), texture.getNormalizedY(), texture.getNormalizedWidth(), texture.getNormalizedHeight(), originX, originY, rotation, scaleX, scaleY);
}

void dr::SpriteBatch::render(const dr::Texture* texture, float x, float y)
{
	render(texture, x, y, 0, 0, 1, 1, 0, 0, 0, 1, 1);
}

void dr::SpriteBatch::render(const dr::Texture* texture, float x, float y, float originX, float originY, float rotation)
{
	render(texture, x, y, 0, 0, 1, 1, originX, originY, rotation, 1, 1);
}

void dr::SpriteBatch::render(const dr::Texture* texture, float x, float y, float originX, float originY, float rotation, float scaleX, float scaleY)
{
	render(texture, x, y, 0, 0, 1, 1, originX, originY, rotation, scaleX, scaleY);
}

void dr::SpriteBatch::render(const dr::Texture* texture, float x, float y, float tx, float ty, float tw, float th)
{
	render(texture, x, y, tx, ty, tw, th, 0, 0, 0, 1, 1);
}

void dr::SpriteBatch::render(const dr::Texture* texture, float x, float y, float tx, float ty, float tw, float th, float originX, float originY, float rotation)
{
	render(texture, x, y, tx, ty, tw, th, originX, originY, rotation, 1, 1);
}

void dr::SpriteBatch::render(const dr::Texture* texture, float x, float y, float tx, float ty, float tw, float th, float originX, float originY, float rotation, float scaleX, float scaleY)
{
	// check if the user has passed in a null texture
	if (texture == nullptr)
	{
		std::cerr << "A null texture was passed into SpriteBatch::render()" << std::endl;
		std::abort();
	}

	// check if the current state of the sprite batch calls for a flush
	if (texture != m_currentTexture || m_draws >= m_maxDraws)
	{
		flush();
		m_currentTexture = texture;
	}

	float verts[8];
	float aw = tw * texture->getWidth(), ah = th * texture->getHeight();

	verts[0] = -originX;
	verts[1] = -originY;

	verts[2] = -originX;
	verts[3] = -originY + ah;

	verts[4] = -originX + aw;
	verts[5] = -originY + ah;

	verts[6] = -originX + aw;
	verts[7] = -originY;

	// apply rotation if needed
	if (rotation != 0)
		m_rotationBuffer.initRotationZ(rotation);

	for (int i = 0; i < 4; i++)
	{
		// scale vertex
		verts[i * 2] *= scaleX;
		verts[i * 2 + 1] *= scaleY;

		// rotate vertex
		if (rotation != 0)
			m_rotationBuffer.mutateMultiply(verts + i * 2, verts + i * 2 + 1);

		// translate vertex
		verts[i * 2] += x + originX;
		verts[i * 2 + 1] += y + originY;
	}

	// push vertex data onto vertex data stack
	
	m_vbo.pushData(verts[0]).pushData(verts[1]);
	m_vbo.pushData(m_tint.getRed()).pushData(m_tint.getGreen()).pushData(m_tint.getBlue()).pushData(m_tint.getAlpha());
	m_vbo.pushData(tx).pushData(ty);

	m_vbo.pushData(verts[2]).pushData(verts[3]);
	m_vbo.pushData(m_tint.getRed()).pushData(m_tint.getGreen()).pushData(m_tint.getBlue()).pushData(m_tint.getAlpha());
	m_vbo.pushData(tx).pushData(ty + th);

	m_vbo.pushData(verts[4]).pushData(verts[5]);
	m_vbo.pushData(m_tint.getRed()).pushData(m_tint.getGreen()).pushData(m_tint.getBlue()).pushData(m_tint.getAlpha());
	m_vbo.pushData(tx + tw).pushData(ty + th);

	m_vbo.pushData(verts[6]).pushData(verts[7]);
	m_vbo.pushData(m_tint.getRed()).pushData(m_tint.getGreen()).pushData(m_tint.getBlue()).pushData(m_tint.getAlpha());
	m_vbo.pushData(tx + tw).pushData(ty);

	m_draws++;

}

void dr::SpriteBatch::begin(void)
{
	if (!m_began)
	{
		m_began = true;
		m_vbo.clearData();
		m_draws = 0;
	}
}

void dr::SpriteBatch::flush(void)
{
	if (m_began && m_currentTexture != nullptr)
	{
		// update shader

		const dr::ShaderProgram* shader = &m_shader;
		if (m_customShader != nullptr)
			shader = m_customShader;

		shader->bindProgram();
		shader->setUniformMatrix4f("u_viewProjection", m_viewProjection);
		
		m_currentTexture->bind();

		m_vbo.flushData();
		m_vao.render(GL_QUADS, m_draws * 4);
		
		// clear the vertex buffer
		m_vbo.clearData();
		m_draws = 0;
	}
}

void dr::SpriteBatch::end(void)
{
	if (m_began)
	{
		flush();
		m_began = false;
	}
	else
	{

	}
}