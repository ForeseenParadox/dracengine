#ifndef RESOURCE_H
#define RESOURCE_H

#include <string>

namespace dr
{
	class Resource
	{
	protected:
		bool m_loaded;
	public:

		Resource(void);
		virtual ~Resource(void);

		bool isLoaded(void) const;

		virtual void load(void) = 0;
		virtual void unload(void) = 0;
	};
}

#endif