#ifndef SHADER_RESOURCE_H
#define SHADER_RESOURCE_H

#include "compositeResource.h"
#include "textResource.h"

namespace dr
{
	const int SHADER_RESOURCE_COUNT = 2;
	class ShaderResource : public dr::CompositeResource
	{

	private:
		dr::TextResource m_vertexShaderSource;
		dr::TextResource m_fragmentShaderSource;
	public:

		ShaderResource(const std::string& vertexShaderPath, const std::string& fragmentShaderPath);
		~ShaderResource(void);

		dr::TextResource getVertexShaderSource(void) const;
		dr::TextResource getFragmentShaderSource(void) const;

		void load(void);
		void unload(void);
	};
}

#endif