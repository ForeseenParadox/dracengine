#ifndef DEBUG_H
#define DEBUG_H

#define DEBUG

#include <iostream>

#ifdef DEBUG

#define ASSERT(CONDITION, MSG) if(!(CONDITION)) { std::cerr << MSG << std::endl; abort();}

#else
#define ASSERT(CONDITION, MSG)

#endif

#endif