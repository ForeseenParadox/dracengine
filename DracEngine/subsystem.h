#ifndef SUBSYSTEM_H
#define SUBSYSTEM_H

namespace dr
{
	struct EngineConfig;

	class Subsystem
	{
	private:
		bool m_updatable;
		bool m_renderable;
	public:
		Subsystem(const dr::EngineConfig& config, bool updatable, bool renderable);
		virtual ~Subsystem(void);

		bool isUpdatable(void) const;
		bool isRenderable(void) const;

		virtual void init(void) = 0;
		virtual void dispose(void) = 0;

		virtual void preUpdate(float delta);
		virtual void update(float delta);
		virtual void preRender(void);
		virtual void render(void);
	};
}
#endif