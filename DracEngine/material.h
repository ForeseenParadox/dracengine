#ifndef MATERIAL_H
#define MATERIAL_H

#include "texture.h"

namespace dr
{
	class Material
	{
	private:
		const dr::Texture* m_texture;
		const dr::Texture* m_normalMap;
		float m_specularCoefficient;
		float m_specularExponent;
	public:
		Material(void);
		Material(const dr::Texture* texture);
		Material(const dr::Texture* texture, const dr::Texture* normalMap);
		Material(const dr::Texture* texture, const dr::Texture* normalMap, float specularCoefficient, float specularExponent);
		Material(const dr::Texture* texture, float specularCoefficient, float specularExponent);
		~Material(void);

		const dr::Texture* getTexture(void) const;
		const dr::Texture* getNormalMap(void) const;
		float getSpecularCoefficient(void) const;
		float getSpecularExponent(void) const;

		void setTexture(const dr::Texture* texture);
		void setNormalMap(const dr::Texture* normalMap);
		void setSpecularCoefficient(float specularCoefficient);
		void setSpecularExponent(float specularExponent);
	};
}

#endif

