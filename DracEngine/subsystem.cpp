#include "subsystem.h"
#include "econfig.h"

dr::Subsystem::Subsystem(const dr::EngineConfig& config, bool updatable, bool renderable) :
m_updatable(updatable),
m_renderable(renderable)
{
}

dr::Subsystem::~Subsystem(void)
{

}

bool dr::Subsystem::isUpdatable(void) const
{
	return m_updatable;
}

bool dr::Subsystem::isRenderable(void) const
{
	return m_renderable;
}

void dr::Subsystem::preUpdate(float delta)
{

}

void dr::Subsystem::update(float delta)
{

}

void dr::Subsystem::preRender(void)
{

}

void dr::Subsystem::render()
{

}