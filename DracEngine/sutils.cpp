#include "sutils.h"
#include <iostream>
#include <sstream>

std::vector<std::string> dr::splitString(const std::string& str, char delim, bool trim)
{
	std::vector<std::string> result;

	int index = -1;

	int startIndex = 0;

	while (++index <= str.length())
	{
		if (index == str.length() || str.at(index) == delim)
		{
			if (!trim)
				result.push_back(str.substr(startIndex, index - startIndex));
			else
				result.push_back(trimString(str.substr(startIndex, index - startIndex)));

			startIndex = index + 1;
		}
	}

	return result;
}

std::vector<std::string> dr::splitString(const std::string& str, char delim)
{
	return splitString(str, delim, false);
}

std::string dr::trimString(const std::string& str)
{

	int beginIndex = 0;
	int endIndex = str.length() - 1;

	while (beginIndex < str.length())
	{
		char c = str.at(beginIndex);
		if (c != '\n' && c != '\t' && c > ' ')
			break;
		beginIndex++;
	}

	while (endIndex >= beginIndex)
	{
		char c = str.at(endIndex);
		if (c != '\n' && c != '\t' && c > ' ')
			break;
		endIndex--;
	}
	return str.substr(beginIndex, endIndex - beginIndex + 1);
}