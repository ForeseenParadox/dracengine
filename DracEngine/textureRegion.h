#ifndef TEXTURE_REGION_H
#define TEXTURE_REGION_H

#include "texture.h"

namespace dr
{
	class TextureRegion
	{
	private:
		const dr::Texture* m_texture;
		float m_x;
		float m_y;
		float m_width;
		float m_height;

		void nullTextureCrash() const;
	public:
		TextureRegion(void);
		TextureRegion(const dr::Texture* texture);
		TextureRegion(const dr::Texture* texture, float x, float y, float w, float h);
		TextureRegion(const dr::Texture* texture, int x, int y, int w, int h);
		TextureRegion(const dr::TextureRegion& other);
		~TextureRegion(void);

		const dr::Texture* getTexture(void) const;
		float getNormalizedX(void) const;
		float getNormalizedY(void) const;
		float getNormalizedWidth(void) const;
		float getNormalizedHeight(void) const;
		int getX(void) const;
		int getY(void) const;
		int getWidth(void) const;
		int getHeight(void) const;

		void setTexture(const dr::Texture* texture);
		void setNormalizedX(float x);
		void setNormalizedY(float y);
		void setNormalizedWidth(float width);
		void setNormalizedHeight(float height);
		void setX(int x);
		void setY(int y);
		void setWidth(int width);
		void setHeight(int height);
		
		TextureRegion& operator=(const dr::TextureRegion& other);
	};
}

#endif

