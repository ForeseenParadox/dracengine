#include "vec2.h"
#include <math.h>
#include "dmath.h"
#include <iostream>

dr::Vec2f::Vec2f(void)
{
	m_x = 0;
	m_y = 0;
}

dr::Vec2f::Vec2f(float x, float y)
{
	m_x = x;
	m_y = y;
}

dr::Vec2f::Vec2f(const dr::Vec2f& other)
{
	m_x = other.m_x;
	m_y = other.m_y;
}

dr::Vec2f::Vec2f(dr::Vec2f&& other)
{
	m_x = other.m_x;
	m_y = other.m_y;
}

dr::Vec2f::~Vec2f(void)
{
	// do nothing
}

float dr::Vec2f::getX(void) const
{
	return m_x;
}

float dr::Vec2f::getY(void) const
{
	return m_y;
}

void dr::Vec2f::setX(float x)
{
	m_x = x;
}

void dr::Vec2f::setY(float y)
{
	m_y = y;
}

float dr::Vec2f::length(void) const
{
	return static_cast<float>(sqrt(m_x * m_x + m_y * m_y));
}

float dr::Vec2f::distance(const dr::Vec2f& other) const
{
	return subtract(other).length();
}

float dr::Vec2f::dot(const dr::Vec2f& other) const
{
	return m_x * other.m_x + m_y * other.m_y;
}

void dr::Vec2f::initZero(void)
{
	m_x = 0;
	m_y = 0;
}

dr::Vec2f* dr::Vec2f::mutateAdd(const dr::Vec2f& other)
{
	m_x += other.m_x;
	m_y += other.m_y;

	return this;
}

dr::Vec2f* dr::Vec2f::mutateAdd(float x, float y)
{
	m_x += x;
	m_y += y;

	return this;
}


dr::Vec2f* dr::Vec2f::mutateSubtract(const dr::Vec2f& other)
{
	m_x -= other.m_x;
	m_y -= other.m_y;

	return this;
}

dr::Vec2f* dr::Vec2f::mutateSubtract(float x, float y)
{
	m_x -= x;
	m_y -= y;

	return this;
}

dr::Vec2f* dr::Vec2f::mutateScale(float scaler)
{
	m_x *= scaler;
	m_y *= scaler;

	return this;
}

dr::Vec2f* dr::Vec2f::mutateScale(float scaleX, float scaleY)
{
	m_x *= scaleX;
	m_y *= scaleY;

	return this;
}

dr::Vec2f* dr::Vec2f::mutateScale(const dr::Vec2f& other)
{
	m_x *= other.m_x;
	m_y *= other.m_y;

	return this;
}

dr::Vec2f* dr::Vec2f::mutateNormalize(void)
{
	#ifdef DR_FAST_MATH
	
	float l = fastInverseSqrt(m_x * m_x + m_y * m_y);

	m_x *= l;
	m_y *= l;

	#else

	float l = length();

	m_x /= l;
	m_y /= l;
	#endif

	return this;
}

dr::Vec2f dr::Vec2f::add(const dr::Vec2f& other) const
{
	return Vec2f(m_x + other.m_x, m_y + other.m_y);
}

dr::Vec2f dr::Vec2f::add(float x, float y) const
{
	return Vec2f(m_x + x, m_y + y);
}

dr::Vec2f dr::Vec2f::subtract(const dr::Vec2f& other) const
{
	return Vec2f(m_x - other.m_x, m_y - other.m_y);
}

dr::Vec2f dr::Vec2f::subtract(float x, float y) const
{
	return Vec2f(m_x - x, m_y - y);
}

dr::Vec2f dr::Vec2f::scale(float scaler) const
{
	return Vec2f(m_x * scaler, m_y * scaler);
}

dr::Vec2f dr::Vec2f::scale(float scaleX, float scaleY) const
{
	return Vec2f(m_x * scaleX, m_y * scaleY);
}

dr::Vec2f dr::Vec2f::scale(const dr::Vec2f& other) const
{
	return Vec2f(m_x * other.m_x, m_y * other.m_y);
}

dr::Vec2f dr::Vec2f::normalize(void)
{

	float l = 0;
#ifdef DR_FAST_MATH

	l = fastInverseSqrt(m_x * m_x + m_y * m_y);
#else

	l = length();
#endif

	return Vec2f(m_x / l, m_y / l);
}

dr::Vec2f dr::Vec2f::operator+(const Vec2f& other)
{
	return add(other);
}

dr::Vec2f dr::Vec2f::operator-(const Vec2f& other)
{
	return subtract(other);
}

dr::Vec2f dr::Vec2f::operator*(const Vec2f& other)
{
	return scale(other);
}

dr::Vec2f dr::Vec2f::operator*(float scaler)
{
	return scale(scaler);
}

dr::Vec2f dr::Vec2f::operator+=(const Vec2f& other)
{
	return *mutateAdd(other);
}

dr::Vec2f dr::Vec2f::operator-=(const Vec2f& other)
{
	return *mutateSubtract(other);
}

dr::Vec2f dr::Vec2f::operator*=(const Vec2f& other)
{
	return *mutateScale(other);
}

dr::Vec2f dr::Vec2f::operator*=(float scaler)
{
	return *mutateScale(scaler);
}

bool dr::Vec2f::operator==(const dr::Vec2f& other) const
{
	if (m_x == other.m_x && m_y == other.m_y)
		return true;
	else
		return false;
}