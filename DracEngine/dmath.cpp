#include "dmath.h"

float dr::fastInverseSqrt(float n)
{
	float startGuess = n * 0.5f;
	int floatBits = *(reinterpret_cast<int*>(&n));
	floatBits = 0x5f3759df - (floatBits >> 1);
	n = *(reinterpret_cast<float*>(&floatBits));
	n = n*(1.5f - startGuess*n*n);
	return n;
}