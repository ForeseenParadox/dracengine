#ifndef COLOR_H
#define COLOR_H

namespace dr
{

	class Color
	{
		float m_r;
		float m_g;
		float m_b;
		float m_a;
	public:

		Color();
		Color(float r, float g, float b, float a);
		Color(float r, float g, float b);
		Color(int r, int g, int b, int a);
		Color(int r, int g, int b);
		Color(const dr::Color& other);

		float getRed(void) const;
		float getGreen(void) const;
		float getBlue(void) const;
		float getAlpha(void) const;
		int getRed255(void) const;
		int getGreen255(void) const;
		int getBlue255(void) const;
		int getAlpha255(void) const;

		void setRed(float r);
		void setGreen(float g);
		void setBlue(float b);
		void setAlpha(float a);
		void setRed255(int r);
		void setGreen255(int g);
		void setBlue255(int b);
		void setAlpha255(int a);

		bool operator==(const dr::Color& other);
		dr::Color& operator=(const dr::Color& other);
	};

	const Color RED(255, 0, 0);
	const Color GREEN(0, 255, 0);
	const Color BLUE(0, 0, 255);
	const Color BLACK(0, 0, 0);
	const Color WHITE(255, 255, 255);

}

#endif