#include "meshComponent.h"
#include "shader.h"
#include "mesh.h"
#include <iostream>

dr::MeshComponent::MeshComponent(dr::ShaderProgram* shader, dr::CameraComponent* camera, dr::Mesh* mesh):
m_shader(shader),
m_camera(camera),
m_mesh(mesh)
{

}

dr::MeshComponent::~MeshComponent(void)
{
	 
}

void dr::MeshComponent::update(float delta)
{

}

void dr::MeshComponent::render(const dr::Transform& transform)
{

	m_shader->bindProgram();
	m_shader->setUniformMatrix4f("u_projection", m_camera->getViewProjectionMatrix());
	m_shader->setUniformMatrix4f("u_transform", transform.getTransform());

	m_mesh->render();
}