#ifndef MESH_H
#define MESH_H

#include "vertexArray.h"
#include "vertexBuffer.h"
#include "indexBuffer.h"
#include "vertexAttrib.h"
#include "objResource.h"

namespace dr
{

	class Mesh
	{
	private:
		dr::VertexArray* m_vao;
		dr::VertexBuffer* m_vbo;
		dr::IndexBuffer* m_ibo;

		int m_attribCount;
		dr::VertexAttrib* m_attribs;

		int m_vertexCount;

	public:
		
		Mesh(void);
		Mesh(int vboSize);
		Mesh(int vboSize, int indexCount);
		Mesh(dr::OBJResource* objResource);
		~Mesh(void);
		
		dr::VertexArray* getVertexArray(void);
		dr::VertexBuffer* getVertexBuffer(void);
		dr::IndexBuffer* getIndexBuffer(void);
		int getAttribCount(void) const;
		dr::VertexAttrib* getVertexAttribs();
		int getVertexCount(void) const;

		void setVertexArray(dr::VertexArray* vao);
		void setVertexBuffer(dr::VertexBuffer* vbo);
		void setIndexBuffer(dr::IndexBuffer* ibo);
		void setVertexAttribs(dr::VertexAttrib* attribs, int attribCount);

		void render(void);
	};
}

#endif