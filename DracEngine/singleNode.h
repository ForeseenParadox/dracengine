#ifndef SINGLE_NODE_H
#define SINGLE_NODE_H

namespace dr
{

	template<class T>
	class SingleNode
	{
	private:
		T* m_value;
		SingleNode* m_next;
	public:

		SingleNode(void);
		SingleNode(T* value, SingleNode* next);
		~SingleNode(void);

		T* getValue(void);
		SingleNode* getNext(void);
		void setValue(T* value);
		void setNext(SingleNode* next);
	};

}

#endif