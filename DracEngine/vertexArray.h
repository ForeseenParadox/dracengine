#ifndef VERTEX_ARRAY_H
#define VERTEX_ARRAY_H

#include "vertexBuffer.h"
#include "vertexAttrib.h"
#include "indexBuffer.h"
#include "GL\glew.h"

namespace dr
{

	class VertexArray
	{
	private:
		GLuint m_handle;

		VertexArray(const dr::VertexArray& other);
		VertexArray& operator=(const dr::VertexArray& other);
	public:

		VertexArray(void);
		~VertexArray(void);

		GLuint getHandle(void) const;
		void bind(void) const;

		void bindBuffer(const dr::VertexBuffer* vbo, const dr::VertexAttrib attribs[], int attribCount);

		void render(GLenum primitive, dr::IndexBuffer& ibo, int vertexCount, int elementCount);
		void render(GLenum primitive, int elementCount);
	};

	void unbindVAO(void);
}

#endif