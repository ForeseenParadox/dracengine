#include "objResource.h"
#include <sstream>
#include <iostream>
#include <string>
#include "sutils.h"
#include <fstream>

dr::OBJResource::OBJResource(const std::string& path) :
dr::SingleResource(path)
{

}

dr::OBJResource::~OBJResource(void)
{

}

std::vector<dr::Vec3f> dr::OBJResource::getVertices(void) const
{
	return m_vertices;
}

std::vector<dr::Vec2f> dr::OBJResource::getTexCoords(void) const
{
	return m_texCoords;
}

std::vector<dr::Vec3f> dr::OBJResource::getNormals(void) const
{
	return m_normals;
}

std::vector<dr::OBJIndex> dr::OBJResource::getIndicies(void) const
{
	return m_indicies;
}

dr::Vec3f dr::OBJResource::getVertex(int index) const
{
	return m_vertices[index];
}

dr::Vec2f dr::OBJResource::getTexCoord(int index) const
{
	return m_texCoords[index];
}

dr::Vec3f dr::OBJResource::getNormal(int index) const
{
	return m_normals[index];
}

dr::OBJIndex dr::OBJResource::getIndex(int index) const
{
	return m_indicies[index];
}

bool dr::OBJResource::isTriangulated(void) const
{
	return m_isTriangulated;
}

bool dr::OBJResource::isUsingVertices(void) const
{
	return m_isUsingVertices;
}

bool dr::OBJResource::isUsingTexCoords(void) const
{
	return m_isUsingTexCoords;
}

bool dr::OBJResource::isUsingNormals(void) const
{
	return m_isUsingNormals;
}

void dr::OBJResource::load(void)
{
	std::ifstream input(getLocation());
	std::string lastString;

	m_isUsingVertices = false;
	m_isUsingTexCoords = false;
	m_isUsingNormals = false;

	while (std::getline(input, lastString))
	{
		// split on space character
		std::vector<std::string> tokens = splitString(lastString, ' ');

		if (tokens.at(0) == "v")
		{
			float x = std::stof(tokens.at(1)), y = std::stof(tokens.at(2)), z = std::stof(tokens.at(3));
			m_vertices.push_back(dr::Vec3f(x, y, z));
		}
		else if (tokens.at(0) == "vn")
		{
			float x = std::stof(tokens.at(1)), y = std::stof(tokens.at(2)), z = std::stof(tokens.at(3));
			m_normals.push_back(dr::Vec3f(x, y, z));
		}
		else if (tokens.at(0) == "f")
		{

			if (tokens.size() > 4)
				m_isTriangulated = false;
			else
				m_isTriangulated = true;

			// temporary index loader
			for (int i = 1; i <= (m_isTriangulated ? 3 : 4); i++)
			{

				// obj indicies start at 1, so subtract 1
				std::string& token = tokens.at(i);
				std::vector<std::string> indexTokens = splitString(token, '/');

				// load the vertex, texCoord and normal indicies if possible
				unsigned int vertexIndex = 0;
				unsigned int texCoordIndex = 0;
				unsigned int normalIndex = 0;

				if (indexTokens.size() > 0 && indexTokens.at(0) != "")
					m_isUsingVertices = true;
				if (indexTokens.size() > 1 && indexTokens.at(1) != "")
					m_isUsingTexCoords = true;
				if (indexTokens.size() > 2 && indexTokens.at(2) != "")
					m_isUsingNormals = true;

				if (m_isUsingVertices)
					vertexIndex = std::stoi(indexTokens.at(0)) - 1;
				if (m_isUsingTexCoords)
					texCoordIndex = std::stoi(indexTokens.at(1)) - 1;
				if (m_isUsingNormals)
					normalIndex = std::stoi(indexTokens.at(2)) - 1;

				m_indicies.push_back(dr::OBJIndex(vertexIndex, texCoordIndex, normalIndex));
			}
		}
	}
}

void dr::OBJResource::unload(void)
{

}