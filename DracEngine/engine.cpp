#include "engine.h"
#include "core.h"
#include "simulation.h"
#include "econfig.h"
#include "sdlsys.h"
#include "window.h"
#include "resourceManager.h"
#include "input.h"
#include "baseGame.h"
#include "SDL\SDL.h"
#include "GL\glew.h"
#include <iostream>

// define static variables
dr::Engine* dr::Engine::s_instance = nullptr;

dr::Engine::Engine(const dr::EngineConfig& config):
m_config(config)
{
	checkInstantiation();
	initEngine(config);
}

dr::Engine::~Engine(void)
{
	// delete core engine components
	delete m_core;
	delete m_simulationManager;
}

void dr::Engine::checkInstantiation(void)
{
	if (s_instance != nullptr)
	{
		throw std::runtime_error("Can not create more than one instance of an Engine during runtime.");
	} else
	{
		s_instance = this;
	}
}

void dr::Engine::processEvent(SDL_Event* e)
{
	if (e->type == SDL_QUIT)
		s_instance->exit(USER_EXIT);
	else
		s_instance->m_inputSystem->processEvent(e);
}

void dr::Engine::updateCallback(float delta)
{
	if (getInstance()->m_postLoad)
	{
		getInstance()->m_game->postLoad();
		getInstance()->m_postLoad = false;
	}

	// update game
	if (getInstance()->getResourceManager()->isLoading())
		getInstance()->m_game->preUpdate(delta);
	else
		getInstance()->m_game->update(delta);

	getInstance()->getCore()->updateSubsystems(delta, getInstance()->getResourceManager()->isLoading());
}

void dr::Engine::renderCallback()
{	
	// clear the screen
	glClearColor(0.0, 0.0, 0.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT);

	// render game
	if (getInstance()->getResourceManager()->isLoading())
		getInstance()->m_game->preRender();
	else
		getInstance()->m_game->render();

	getInstance()->getCore()->renderSubsystems(getInstance()->getResourceManager()->isLoading());
}

void dr::Engine::loadingCompleteCallback(void)
{
	getInstance()->m_postLoad = true;
}

dr::Engine* dr::Engine::getInstance(void)
{
	return s_instance;
}

dr::SDLSys* dr::Engine::getSDLSystem(void) const
{
	return m_sdlSystem;
}

dr::Window* dr::Engine::getWindow(void) const
{
	return m_window;
}

dr::ResourceManager* dr::Engine::getResourceManager(void) const
{
	return m_resourceManager;
}

dr::Input* dr::Engine::getInputSystem(void) const
{
	return m_inputSystem;
}

dr::Core* dr::Engine::getCore(void) const
{
	return m_core;
}

dr::SimulationManager* dr::Engine::getSimulationManager(void) const
{
	return m_simulationManager;
}

dr::EngineConfig& dr::Engine::getConfiguration(void)
{
	return m_config;
}

void dr::Engine::initEngine(const dr::EngineConfig& config)
{
	m_config = config;

	m_core = new Core(SUBSYSTEM_COUNT, UPDATABLE_SUBSYSTEM_COUNT, RENDERABLE_SUBSYSTEM_COUNT);
	m_simulationManager = new SimulationManager(&updateCallback, &renderCallback, m_config.getMaxUps(), m_config.getMaxFps());

	m_sdlSystem = new SDLSys(config, processEvent);
	m_window = new Window(config);
	m_resourceManager = new ResourceManager(config, &loadingCompleteCallback);
	m_inputSystem = new Input(config);

	// all subsystems
	m_core->insertSubsystem(0, m_sdlSystem);
	m_core->insertSubsystem(1, m_window);
	m_core->insertSubsystem(2, m_resourceManager);
	m_core->insertSubsystem(3, m_inputSystem);

	// updatable subsystems
	m_core->insertUpdatableSubsystem(0, m_sdlSystem);
	m_core->insertUpdatableSubsystem(1, m_inputSystem);

	// renderable subsystems
	m_core->insertRenderableSubsystem(0, m_window);

	// initialize all of the engine subsystems
	m_core->initSubsystems();

	// load default engine resources here
	{

	}
}

void dr::Engine::launch(dr::BaseGame* game)
{
	// load game
	m_game = game;
	m_game->load();

	// load all inserted resources
	m_resourceManager->loadAllResources();
	
	// start the engine simulation
	m_simulationManager->beginSimulation();
}

void dr::Engine::exit(int exitReason)
{
	if (exitReason == CRASH_EXIT)
	{
		// write crash report
	} else if (exitReason == USER_EXIT)
	{
		// dispose game
		m_game->dispose();

		// unload resources
		m_resourceManager->unloadAllResources();
	
		// end
		m_simulationManager->endSimulation();
		m_core->disposeSubsystems();
	}
}