#ifndef COMPOSITE_RESOURCE_H
#define COMPOSITE_RESOURCE_H

#include "resource.h"

namespace dr
{
	class CompositeResource : public Resource
	{
	private:
		int m_childrenCount;
		Resource** m_children;
	public:
		CompositeResource(void);
		CompositeResource(Resource** children, int childrenCount);
		virtual ~CompositeResource(void);

		int getChildrenCount(void);
		Resource** getChildren(void) const;

		void setChildrenCount(int count);
		void setChildren(Resource** chilren);

		virtual void load(void);
		virtual void unload(void);
	};
}

#endif