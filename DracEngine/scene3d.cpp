#include "scene3d.h"

dr::Scene3D::Scene3D(void):
m_rootObject(nullptr)
{
}

dr::Scene3D::Scene3D(dr::GameObject* rootObject) :
m_rootObject(rootObject)
{
}

dr::Scene3D::~Scene3D(void)
{

}

dr::GameObject* dr::Scene3D::getRootObject(void)
{
	return m_rootObject;
}

void dr::Scene3D::setRootObject(dr::GameObject* rootObject)
{
	m_rootObject = rootObject;
}