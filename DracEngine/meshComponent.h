#ifndef MESH_COMPONENT_H
#define MESH_COMPONENT_H

#include "gameComponent.h"
#include "mat4.h"
#include "cameraComponent.h"

namespace dr
{

	class ShaderProgram;
	class Mesh;

	class MeshComponent : public dr::GameComponent
	{
	private:
		dr::ShaderProgram* m_shader;
		dr::CameraComponent* m_camera;
		dr::Mesh* m_mesh;
	public:

		MeshComponent(dr::ShaderProgram* shader, dr::CameraComponent* camera, dr::Mesh* mesh);
		~MeshComponent(void);

		void update(float delta);
		void render(const dr::Transform& transform);
	};
}

#endif