#ifndef SDL_SYS_H
#define SDL_SYS_H

#include "subsystem.h"
#include "econfig.h"

union SDL_Event;

namespace dr
{
	struct EngineConfig;

	class SDLSys : public Subsystem
	{
	private:

		// callback function for event handling
		void(*m_eventCallback)(SDL_Event*);

		void pollEvents(void);

	public:
		SDLSys(const dr::EngineConfig& config, void(*eventCallback)(SDL_Event*));
		~SDLSys(void);

		void init(void);
		void preUpdate(float delta);
		void update(float delta);
		void dispose(void);

	};
}

#endif