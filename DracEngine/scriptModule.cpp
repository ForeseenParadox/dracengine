#include "scriptModule.h"

dr::ScriptModule::ScriptModule(void)
{

}

dr::ScriptModule::~ScriptModule(void)
{

}

const std::string& dr::ScriptModule::getName(void) const
{
	return m_name;
}

std::map<std::string, dr::ScriptFunction>& dr::ScriptModule::getFunctions(void)
{
	return m_functions;
}

dr::ScriptFunction dr::ScriptModule::getFunction(const std::string& functionName) const
{
	return m_functions.at(functionName);
}

void dr::ScriptModule::setName(const std::string& name)
{
	m_name = name;
}

void dr::ScriptModule::registerFunction(const std::string& name, dr::ScriptFunction& function)
{
	m_functions.insert(std::pair<std::string, dr::ScriptFunction>(name, function));
}

void dr::ScriptModule::executeFunction(const std::string& name)
{
	// code for executing function during runtime
}