#include "shapeBatch.h"
#include "engine.h"
#include "GL\glew.h"
#include "shader.h"
#include "dMath.h"
#include "window.h"
#include "engine.h"
#include <math.h>
#include <iostream>

const int dr::ShapeBatch::DEFAULT_MAX_VERTEX_COUNT = 100000;
const int dr::ShapeBatch::ATTRIB_COUNT = 2;
dr::VertexAttrib dr::ShapeBatch::ATTRIBS[ATTRIB_COUNT] = { dr::VertexAttrib(0, 2, std::string("a_position")), dr::VertexAttrib(1, 4, std::string("a_color")) };

dr::ShapeBatch::ShapeBatch(void):
m_vbo(DEFAULT_MAX_VERTEX_COUNT),
m_maxVertices(DEFAULT_MAX_VERTEX_COUNT),
m_began(false),
m_customShader(nullptr)
{
	initShapeBatch();
}

dr::ShapeBatch::ShapeBatch(int maxVertices) :
m_vbo(maxVertices),
m_maxVertices(maxVertices),
m_began(false),
m_customShader(nullptr)
{
	initShapeBatch();
}

dr::ShapeBatch::ShapeBatch(ShaderProgram* shader) :
m_vbo(DEFAULT_MAX_VERTEX_COUNT),
m_maxVertices(DEFAULT_MAX_VERTEX_COUNT),
m_began(false),
m_customShader(shader)
{
	initShapeBatch();
}

dr::ShapeBatch::ShapeBatch(ShaderProgram* shader, int maxVertices) :
m_vbo(maxVertices),
m_maxVertices(maxVertices),
m_began(false),
m_customShader(shader)
{
	initShapeBatch();
}

void dr::ShapeBatch::initShapeBatch()
{
	// create first time view projection matrix
	dr::Window* wnd = Engine::getInstance()->getWindow();
	m_viewProjection.initOrthographicProjection(0, wnd->getWidth(), 0, wnd->getHeight(), -1, 1);

	// create default shaders
	std::string vertexSource =
		std::string("#version 110\n") +
		std::string("uniform mat4 u_viewProjection;\n") +
		std::string("attribute vec2 a_position;\n") +
		std::string("attribute vec4 a_color;\n") +
		std::string("varying vec4 v_color;\n") +
		std::string("void main()\n") +
		std::string("{\n") +
		std::string("	gl_Position = u_viewProjection * vec4(a_position, 0.0, 1.0);\n") +
		std::string("	v_color = a_color;\n") +
		std::string("}\n");

	std::string fragmentSource =
		std::string("#version 110\n") +
		std::string("varying vec4 v_color;\n") +
		std::string("void main()\n") +
		std::string("{\n") +
		std::string("	gl_FragColor = v_color;\n") +
		std::string("}\n");

	m_shader.bindAttribs(ATTRIBS, ATTRIB_COUNT);
	m_shader.compileAndLink(vertexSource, fragmentSource);

	// bind vertex buffer and attributes to vertex array
	m_vao.bindBuffer(&m_vbo, ATTRIBS, ATTRIB_COUNT);
}

void dr::ShapeBatch::checkFlush(GLenum requestedPrimitive)
{
	if (m_currentPrimitive != requestedPrimitive)
	{
		flush();
		m_currentPrimitive = requestedPrimitive;
	}
}

int dr::ShapeBatch::getMaxVertices(void) const
{
	return m_maxVertices;
}

GLenum dr::ShapeBatch::getCurrentPrimitive(void) const
{
	return m_currentPrimitive;
}

dr::Color& dr::ShapeBatch::getColor(void)
{
	return m_color;
}

bool dr::ShapeBatch::hasBegan(void) const
{
	return m_began;
}

bool dr::ShapeBatch::isUsingCustomShader(void) const
{
	return m_customShader != nullptr;
}

void dr::ShapeBatch::setColor(const dr::Color& color)
{
	m_color = color;
}

void dr::ShapeBatch::setCustomShader(dr::ShaderProgram* customShader)
{
	m_customShader = customShader;
}

void dr::ShapeBatch::setViewProjectionMatrix(const dr::Matrix4f& viewProjection)
{
	m_viewProjection = viewProjection;
}

void dr::ShapeBatch::renderLine(float x1, float y1, float x2, float y2)
{
	checkFlush(GL_LINES);

	m_vbo.pushData(x1).pushData(y1);
	m_vbo.pushData(m_color.getRed()).pushData(m_color.getGreen()).pushData(m_color.getBlue()).pushData(m_color.getAlpha());
	m_vbo.pushData(x2).pushData(y2);
	m_vbo.pushData(m_color.getRed()).pushData(m_color.getGreen()).pushData(m_color.getBlue()).pushData(m_color.getAlpha());

	m_vertices += 2;
}

void dr::ShapeBatch::renderRect(float x, float y, float w, float h)
{
	
	float vertexBuffer[8];

	vertexBuffer[0] = x;
	vertexBuffer[1] = y;

	vertexBuffer[2] = x;
	vertexBuffer[3] = y + h;

	vertexBuffer[4] = x + w;
	vertexBuffer[5] = y + h;

	vertexBuffer[6] = x + w;
	vertexBuffer[7] = y;

	renderShape(vertexBuffer, 4);
}

void dr::ShapeBatch::renderRect(float x, float y, float w, float h, float ox, float oy, float rotation, float scaleX, float scaleY)
{
	float vertexBuffer[8];

	vertexBuffer[0] = -ox;
	vertexBuffer[1] = -oy;

	vertexBuffer[2] = -ox;
	vertexBuffer[3] = h - oy;

	vertexBuffer[4] = w - ox;
	vertexBuffer[5] = h - oy;

	vertexBuffer[6] = w - ox;
	vertexBuffer[7] = -oy;

	// init rotation
	Matrix4f rotationBuffer;
	if (rotation != 0)
		rotationBuffer.initRotationZ(rotation);

	for (int i = 0; i < 8; i +=2)
	{		
		// mutate the vertices for the rotated rect
		if (rotation != 0)
			rotationBuffer.mutateMultiply(vertexBuffer + i, vertexBuffer + i + 1);

		vertexBuffer[i] += x + ox;
		vertexBuffer[i + 1] += y + oy;
	}

	renderShape(vertexBuffer, 4);
}

void dr::ShapeBatch::renderFilledRect(float x, float y, float w, float h)
{
	renderFilledRect(x, y, w, h, 0, 0, 0, 1, 1);
}

void dr::ShapeBatch::renderFilledRect(float x, float y, float w, float h, float ox, float oy, float rotation, float scaleX, float scaleY)
{

	checkFlush(GL_QUADS);

	float vertexBuffer[8];
	vertexBuffer[0] = -ox;
	vertexBuffer[1] = -oy;

	vertexBuffer[2] = -ox;
	vertexBuffer[3] = h - oy;

	vertexBuffer[4] = w - ox;
	vertexBuffer[5] = h - oy;

	vertexBuffer[6] = w - ox;
	vertexBuffer[7] = -oy;

	// init rotation
	Matrix4f rotationBuffer;
	if (rotation != 0)
		rotationBuffer.initRotationZ(rotation);

	for (int i = 0; i < 8; i += 2)
	{
		// mutate the vertices for the rotated rect
		if (rotation != 0)
			rotationBuffer.mutateMultiply(vertexBuffer + i, vertexBuffer + i + 1);

		// push the data onto the vertex stack
		m_vbo.pushData(vertexBuffer[i] * scaleX + x + ox).pushData(vertexBuffer[i + 1] * scaleY + y + oy);
		m_vbo.pushData(m_color.getRed()).pushData(m_color.getGreen()).pushData(m_color.getBlue()).pushData(m_color.getAlpha());
	}

	m_vertices += 4;
}

void dr::ShapeBatch::renderCircle(float x, float y, float r)
{
	checkFlush(GL_LINES);

	static const float CIRCLE_RES = 0.1f;

	for (float angle = 0; angle <= 2 * dr::PI; angle += CIRCLE_RES)
	{
		m_vbo.pushData(x + cos(angle) * r).pushData(y + sin(angle) * r);
		m_vbo.pushData(m_color.getRed()).pushData(m_color.getGreen()).pushData(m_color.getBlue()).pushData(m_color.getAlpha());
		m_vbo.pushData(x + cos(angle + CIRCLE_RES) * r).pushData(y + sin(angle + CIRCLE_RES) * r);
		m_vbo.pushData(m_color.getRed()).pushData(m_color.getGreen()).pushData(m_color.getBlue()).pushData(m_color.getAlpha());

		m_vertices += 2;
	}
}

void dr::ShapeBatch::renderShape(float* xPoints, float* yPoints, int pointCount)
{

	if (pointCount == 2)
		renderLine(xPoints[0], yPoints[0], xPoints[1], yPoints[1]);
	else if (pointCount > 2)
	{
		checkFlush(GL_LINES);

		for (int i = 0; i < pointCount; i++)
		{
			float x1 = xPoints[i % pointCount], y1 = yPoints[i % pointCount];
			float x2 = xPoints[(i + 1) % pointCount], y2 = yPoints[(i + 1) % pointCount];

			m_vbo.pushData(x1).pushData(y1);
			m_vbo.pushData(m_color.getRed()).pushData(m_color.getGreen()).pushData(m_color.getBlue()).pushData(m_color.getAlpha());
			m_vbo.pushData(x2).pushData(y2);
			m_vbo.pushData(m_color.getRed()).pushData(m_color.getGreen()).pushData(m_color.getBlue()).pushData(m_color.getAlpha());
		}

		m_vertices += 2 * pointCount;
	}

}

void dr::ShapeBatch::renderShape(float* pointsInterlaced, int pointCount)
{
	if (pointCount == 2)
		renderLine(pointsInterlaced[0], pointsInterlaced[1], pointsInterlaced[2], pointsInterlaced[3]);
	else if (pointCount > 2)
	{
		checkFlush(GL_LINES);

		for (int i = 0; i < pointCount; i++)
		{
			float x1 = pointsInterlaced[i*2 % (pointCount*2)], y1 = pointsInterlaced[(i*2 + 1) % (pointCount*2)];
			float x2 = pointsInterlaced[(i*2 + 2) % (pointCount*2)], y2 = pointsInterlaced[(i*2 + 3) % (pointCount*2)];

			m_vbo.pushData(x1).pushData(y1);
			m_vbo.pushData(m_color.getRed()).pushData(m_color.getGreen()).pushData(m_color.getBlue()).pushData(m_color.getAlpha());
			m_vbo.pushData(x2).pushData(y2);
			m_vbo.pushData(m_color.getRed()).pushData(m_color.getGreen()).pushData(m_color.getBlue()).pushData(m_color.getAlpha());
		}

		m_vertices += 2 * pointCount;
	}
}

void dr::ShapeBatch::begin(void)
{
	if (!m_began)
	{
		m_began = true;
		m_vbo.clearData();
		m_vertices = 0;
	} 
	else
	{

	}
}

void dr::ShapeBatch::flush(void)
{
	if (m_began && m_currentPrimitive != 0)
	{

		dr::ShaderProgram* shader = &m_shader;
		if (m_customShader != nullptr)
			shader = m_customShader;

		shader->bindProgram();
		shader->setUniformMatrix4f("u_viewProjection", m_viewProjection);
		
		m_vbo.flushData();
		m_vao.render(m_currentPrimitive, m_vertices);
		
		// clear the vertex buffer
		m_vbo.clearData();
		m_vertices = 0;
	}
}

void dr::ShapeBatch::end(void)
{
	if (m_began)
	{
		flush();
		m_began = false;
	}
	else
	{

	}
}