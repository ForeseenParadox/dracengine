#include "transform.h"

dr::Transform::Transform(void)
{
	m_translation.initIdentity();
	m_rotation.initIdentity();
	m_scale.initIdentity();
}

dr::Transform::Transform(const dr::Transform& other):
m_translation(other.getTranslation()),
m_rotation(other.getRotation()),
m_scale(other.getScale())
{

}

dr::Transform::~Transform(void)
{

}

dr::Matrix4f dr::Transform::getTranslation(void) const
{
	return m_translation;
}

dr::Matrix4f dr::Transform::getRotation(void) const
{
	return m_rotation;
}

dr::Matrix4f dr::Transform::getScale(void) const
{
	return m_scale;
}

dr::Matrix4f dr::Transform::getTransform(void) const
{
	return m_translation.multiply(m_rotation.multiply(m_scale));
}

void dr::Transform::setTranslation(float tx, float ty, float tz)
{
	m_translation.initTranslation(tx, ty, tz);
}

void dr::Transform::setRotation(float rx, float ry, float rz)
{
	dr::Matrix4f rotX, rotY, rotZ;

	rotX.initRotationX(rx);
	rotY.initRotationY(ry);
	rotZ.initRotationZ(rz);

	m_rotation = rotZ.multiply(rotY.multiply(rotX));
}

void dr::Transform::setScale(float sx, float sy, float sz)
{
	m_scale.initScale(sx, sy, sz);
}

void dr::Transform::setTranslation(dr::Matrix4f translation)
{
	m_translation = translation;
}

void dr::Transform::setRotation(dr::Matrix4f rotation)
{
	m_rotation = rotation;
}

void dr::Transform::setScale(dr::Matrix4f scale)
{
	m_scale = scale;
}