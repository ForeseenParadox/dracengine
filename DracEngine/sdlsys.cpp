#include "sdlsys.h"
#include "SDL\SDL.h"
#include <iostream>
#include "GL\glew.h"
#include "SDL\SDL_image.h"

dr::SDLSys::SDLSys(const dr::EngineConfig& config, void(*eventCallback)(SDL_Event*)):
m_eventCallback(eventCallback),
dr::Subsystem(config, true, false)
{
}

dr::SDLSys::~SDLSys(void)
{

}

void dr::SDLSys::init(void)
{
	// error check
	if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
	{
		std::cerr << "SDL failed to initialize with error code " << SDL_GetError() << std::endl;
	}
	else
	{
		std::cout << "SDL initialized successfully" << std::endl;
	}

	int flags = IMG_INIT_PNG;

	if (flags & IMG_Init(IMG_INIT_PNG) != flags)
	{
		std::cerr << "SDL image failed to initialize properly" << std::endl;
	}
	else
	{
		std::cout << "SDL image initialized successfully" << std::endl;
	}
}

void dr::SDLSys::preUpdate(float delta)
{
	pollEvents();
}

void dr::SDLSys::update(float delta)
{
	pollEvents();
}

void dr::SDLSys::pollEvents(void)
{
	SDL_Event e;

	while (SDL_PollEvent(&e))
	{
		// invoke the callback function on the received event
		m_eventCallback(&e);
	}
}

void dr::SDLSys::dispose(void)
{
	SDL_Quit();
	IMG_Quit();
}