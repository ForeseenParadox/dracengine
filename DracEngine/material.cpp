#include "material.h"

dr::Material::Material(void):
m_texture(nullptr),
m_normalMap(nullptr),
m_specularCoefficient(1),
m_specularExponent(1)
{
}

dr::Material::Material(const dr::Texture* texture):
m_texture(texture),
m_normalMap(nullptr),
m_specularCoefficient(1),
m_specularExponent(1)
{
}

dr::Material::Material(const dr::Texture* texture, const dr::Texture* normalMap):
m_texture(texture),
m_normalMap(normalMap),
m_specularCoefficient(1),
m_specularExponent(1)
{
}

dr::Material::Material(const dr::Texture* texture, const dr::Texture* normalMap, float specularCoefficient, float specularExponent) :
m_texture(texture),
m_normalMap(normalMap),
m_specularCoefficient(specularCoefficient),
m_specularExponent(specularExponent)
{
}

dr::Material::Material(const dr::Texture* texture, float specularCoefficient, float specularExponent):
m_texture(texture),
m_normalMap(nullptr),
m_specularCoefficient(specularCoefficient),
m_specularExponent(specularExponent)
{
}

dr::Material::~Material(void)
{

}

const dr::Texture* dr::Material::getTexture(void) const
{
	return m_texture;
}

const dr::Texture* dr::Material::getNormalMap(void) const
{
	return m_normalMap;
}

float dr::Material::getSpecularCoefficient(void) const
{
	return m_specularCoefficient;
}

float dr::Material::getSpecularExponent(void) const
{
	return m_specularExponent;
}

void dr::Material::setTexture(const dr::Texture* texture)
{
	m_texture = texture;
}

void dr::Material::setNormalMap(const dr::Texture* normalMap)
{
	m_normalMap = normalMap;
}

void dr::Material::setSpecularCoefficient(float specularCoefficient)
{
	m_specularCoefficient = specularCoefficient;
}

void dr::Material::setSpecularExponent(float specularExponent)
{
	m_specularExponent = specularExponent;
}