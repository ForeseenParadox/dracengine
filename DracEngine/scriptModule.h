#ifndef SCRIPT_MODULE_H
#define SCRIPT_MODULE_H

#include <map>
#include <array>
#include "scriptFunction.h"

namespace dr
{

	class ScriptModule
	{

	private:
		std::string m_name;
		std::map<std::string, dr::ScriptFunction> m_functions;
	public:

		ScriptModule(void);
		~ScriptModule(void);

		const std::string& getName(void) const;
		std::map<std::string, dr::ScriptFunction>& getFunctions(void);
		dr::ScriptFunction getFunction(const std::string& functionName) const;

		void setName(const std::string& name);
		void registerFunction(const std::string& name, dr::ScriptFunction& function);
		
		void executeFunction(const std::string& name);
	};

}

#endif