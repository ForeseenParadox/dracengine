#include "textResource.h"
#include <fstream>
#include <string>
#include <iostream>

dr::TextResource::TextResource(const std::string& location) :
SingleResource(location)
{

}

std::string& dr::TextResource::getLoadedText(void) const
{
	return const_cast<std::string&>(m_loadedText);
}

void dr::TextResource::load(void)
{
	// reset the loaded text in case it has been modified

	m_loadedText= "";

	std::ifstream in(getLocation());
	std::string& lastLine = std::string();

	while (std::getline(in, lastLine))
	{
		m_loadedText.append(lastLine + '\n');
	}
	in.close();

	m_loaded = true;
}

void dr::TextResource::unload(void)
{
	// reset loaded text
	m_loadedText = "";
}