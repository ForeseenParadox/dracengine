#include "texture.h"
#include <iostream>

dr::Texture::Texture(void):
m_handle(0),
m_width(0),
m_height(0),
m_isLoaded(false)
{
	glGenTextures(1, &m_handle);
}

dr::Texture::Texture(const dr::TextureResource& resource):
m_handle(0),
m_width(0),
m_height(0),
m_isLoaded(false)
{
	glGenTextures(1, &m_handle);
	load(resource);
}

dr::Texture::~Texture(void)
{
	glDeleteTextures(1, &m_handle);
}

GLuint dr::Texture::getHandle(void) const
{
	return m_handle;
}

int dr::Texture::getWidth(void) const
{
	return m_width;
}

int dr::Texture::getHeight(void) const
{
	return m_height;
}

bool dr::Texture::isLoaded(void) const
{
	return m_isLoaded;
}

void dr::Texture::bind(void) const
{
	glBindTexture(GL_TEXTURE_2D, m_handle);
}

void dr::Texture::loadEmptyTexture(int width, int height, GLenum filter)
{
	m_width = width;
	m_height = height;

	bind();

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filter);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filter);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
}

void dr::Texture::load(const dr::TextureResource& resource)
{
	load(resource, GL_LINEAR);
}

void dr::Texture::load(const dr::TextureResource& resource, GLenum filter)
{
	m_width = resource.getWidth();
	m_height = resource.getHeight();

	bind();

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filter);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filter);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, resource.getWidth(), resource.getHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, resource.getPixelData());
}