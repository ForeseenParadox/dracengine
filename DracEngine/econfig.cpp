#include "econfig.h"
#include "SDL\SDL_video.h"

dr::EngineConfig::EngineConfig(void) :
m_maxFps(60),
m_maxUps(60),
m_windowInfo()
{
}

dr::EngineConfig::EngineConfig(int maxUps, int maxFps, const dr::WindowInfo& windowInfo):
m_maxFps(maxFps),
m_maxUps(maxUps),
m_windowInfo(windowInfo)
{
}

dr::EngineConfig::EngineConfig(const dr::EngineConfig& other):
m_maxFps(other.m_maxFps),
m_maxUps(other.m_maxUps),
m_windowInfo(other.m_windowInfo)
{
}

dr::EngineConfig::~EngineConfig(void)
{
}

int dr::EngineConfig::getMaxUps(void) const
{
	return m_maxUps;
}

int dr::EngineConfig::getMaxFps(void) const
{
	return m_maxFps;
}

dr::WindowInfo dr::EngineConfig::getWindowInfo(void) const
{
	return m_windowInfo;
}

void dr::EngineConfig::setMaxUps(int maxUps)
{
	m_maxUps = maxUps;
}

void dr::EngineConfig::setMaxFps(int maxFps)
{
	m_maxFps = maxFps;
}

void dr::EngineConfig::setWindowInfo(const dr::WindowInfo& windowInfo)
{
	m_windowInfo = windowInfo;
}

dr::EngineConfig& dr::EngineConfig::operator=(const dr::EngineConfig& other)
{
	m_maxFps = other.m_maxFps;
	m_maxUps = other.m_maxUps;
	m_windowInfo = other.m_windowInfo;
	return *this;
}