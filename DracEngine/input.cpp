#include "input.h"
#include "SDL\SDL.h"
#include "engine.h"
#include "window.h"
#include <iostream>

dr::Input::Input(const dr::EngineConfig& config):
dr::Subsystem(config, true, false)
{

}

dr::Input::~Input(void)
{

}

void dr::Input::init(void)
{

}

void dr::Input::dispose(void)
{

}

bool dr::Input::isKeyPressed(int keyCode)
{
	return m_keysPressed[keyCode];
}

bool dr::Input::isButtonPressed(int buttonCode)
{
	return m_buttonsPressed[buttonCode];
}

int dr::Input::getMouseX(void)
{
	return m_mouseX;
}

int dr::Input::getMouseY(void)
{
	return m_mouseY;
}

dr::Vec2f dr::Input::getRelativeMousePosition(void)
{
	int x = 0, y = 0;
	SDL_GetRelativeMouseState(&x, &y);
	return dr::Vec2f(x, y);
}

int dr::Input::getDelteMouseX(void)
{
	if (m_lastMouseX == -1)
		m_lastMouseX = m_mouseX;
	int temp = m_lastMouseX;
	m_lastMouseX = m_mouseX;
	return m_mouseX - temp;
}

int dr::Input::getDelteMouseY(void)
{
	if (m_lastMouseY == -1)
		m_lastMouseY = m_mouseY;
	int temp = m_lastMouseY;
	m_lastMouseY = m_mouseY;
	return m_mouseY - temp;
}

void dr::Input::resetMouseDeltaX(void)
{
	m_lastMouseX = m_mouseX;
}

void dr::Input::resetMouseDeltaY(void)
{
	m_lastMouseY = m_mouseY;
}

void dr::Input::processEvent(SDL_Event* ev)
{

	if (ev->type == SDL_KEYDOWN)
		m_keysPressed[ev->key.keysym.scancode] = true;
	else if (ev->type == SDL_KEYUP)
		m_keysPressed[ev->key.keysym.scancode] = false;
	else if (ev->type == SDL_MOUSEBUTTONDOWN)
		m_keysPressed[ev->button.button] = true;
	else if (ev->type == SDL_MOUSEBUTTONUP)
		m_keysPressed[ev->button.button] = false;
	else if (ev->type == SDL_MOUSEMOTION)
	{
		m_mouseX = ev->motion.x;
		m_mouseY = dr::Engine::getInstance()->getWindow()->getHeight() - ev->motion.y;
	}
}
