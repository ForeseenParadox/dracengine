#include "simulation.h"
#include "core.h"
#include <thread>
#include <chrono>
#include "SDL\SDL.h"
#include <iostream>

dr::SimulationManager::SimulationManager(void(*updateCallback)(float), void(*renderCallback)(void), unsigned int maxUps, unsigned int maxFps)
{
	m_updateCallback = updateCallback;
	m_renderCallback = renderCallback;
	m_upsCap = maxUps;
	m_fpsCap = maxFps;
	m_running = false;
}

dr::SimulationManager::~SimulationManager(void)
{

}

unsigned int dr::SimulationManager::getUps(void) const
{
	return m_ups;
}

unsigned int dr::SimulationManager::getFps(void) const
{
	return m_fps;
}

unsigned int dr::SimulationManager::getUpsCap(void) const
{
	return m_upsCap;
}

unsigned int dr::SimulationManager::getFpsCap(void) const
{
	return m_fpsCap;
}

bool dr::SimulationManager::isRunning(void) const
{
	return m_running;
}

void dr::SimulationManager::setUpsCap(unsigned int maxUps)
{
	m_upsCap = maxUps;
}

void dr::SimulationManager::setFpsCap(unsigned int maxFps)
{
	m_fpsCap = maxFps;
}

void dr::SimulationManager::beginSimulation(void)
{
	// run the simulation
	m_running = true;
	run();
}

void dr::SimulationManager::run(void)
{
	// simulation loop

	const double ONE_SECOND = static_cast<double>(1e3);
	
	double lastTime = static_cast<double>(SDL_GetTicks());
	double thisTime = static_cast<double>(SDL_GetTicks());

	double lastUpdate = 0;

	const int TIMER_COUNT = 3;
	double timers[TIMER_COUNT];

	double delta = 0;

	unsigned int updates = 0;
	unsigned int frames = 0;

	// give the timers values of 0
	for (int i = 0; i < TIMER_COUNT; i++)
		timers[i] = 0;

	while (m_running)
	{

		thisTime = static_cast<double>(SDL_GetTicks());
		delta = thisTime - lastTime;
		lastTime = thisTime;

		for (int i = 0; i < TIMER_COUNT; i++)
			timers[i] += delta;

		if (m_running)
		{
			if (m_upsCap < 1.0)
			{
				// convert delta into seconds
				update(static_cast<float>(delta / ONE_SECOND));
				timers[0] = 0.0;
				updates++;
			}
			else
			{
				while (timers[0] > (ONE_SECOND / m_upsCap))
				{
					float updateDelta = static_cast<float>((SDL_GetTicks() - lastUpdate) / ONE_SECOND);
					lastUpdate = static_cast<double>(SDL_GetTicks());

					update(updateDelta);
					timers[0] -= (ONE_SECOND / m_upsCap);
					updates++;
				}
			}
		}

		if (m_running)
		{
			if (m_fpsCap < 1.0)
			{
				render();
				timers[1] = 0.0;
				frames++;
			}
			else if (timers[1] >(ONE_SECOND / m_fpsCap))
			{
				render();
				timers[1] -= (ONE_SECOND / m_fpsCap);
				frames++;
			} 
			else
			{
				// sleep all of the extra time we can before the next frame so we're not in a busy loop
				double estimiatedSleepTime = (ONE_SECOND / m_fpsCap) - timers[1];
				std::this_thread::sleep_for(std::chrono::milliseconds(static_cast<unsigned int>(estimiatedSleepTime * THREAD_SLEEP_MASK)));
			}
		}

		if (timers[2] > ONE_SECOND && m_running)
		{
			m_fps = frames;
			m_ups = updates;
			
			std::cout << m_fps << std::endl;
			updates = 0;
			frames = 0;
			
			timers[2] = 0;
		}	

	}
}

void dr::SimulationManager::update(float delta)
{
	m_updateCallback(delta);
}

void dr::SimulationManager::render(void)
{
	m_renderCallback();
}

void dr::SimulationManager::endSimulation(void)
{
	m_running = false;
}