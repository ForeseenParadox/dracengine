#ifndef S_UTILS
#define S_UTILS

#include <vector>
#include <string>

namespace dr
{
	std::vector<std::string> splitString(const std::string& str, char delim, bool trim);
	std::vector<std::string> splitString(const std::string& str, char delim);
	std::string trimString(const std::string& str);
}

#endif