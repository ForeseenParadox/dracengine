#ifndef TEXT_FILE_H
#define TEXT_FILE_H

#include "singleResource.h"

namespace dr
{
	class TextResource : public dr::SingleResource
	{
		private:
			std::string m_loadedText;
		public:
			TextResource(const std::string& location);

			std::string& getLoadedText(void) const;
		
			void load(void);
			void unload(void);
	};
}

#endif