#ifndef FREE_LOOK_COMPONENT_H
#define FREE_LOOK_COMPONENT_H

#include "gameComponent.h"
#include "mat4.h"
#include "cameraComponent.h"

namespace dr
{

	const float MOVE_SPEED = 0.01f;

	class FreeLookComponent : public dr::GameComponent
	{
	private:

		dr::CameraComponent* m_camera;

		float m_rotX;
		float m_rotY;

		float m_tx;
		float m_ty;
		float m_tz;
	public:
		FreeLookComponent(dr::CameraComponent* m_camera);
		~FreeLookComponent(void);

		void update(float delta);
	};

}

#endif