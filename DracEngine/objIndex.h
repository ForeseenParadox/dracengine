#ifndef OBJ_INDEX_H
#define OBJ_INDEX_H

namespace dr
{

	class OBJIndex
	{
	private:
		unsigned int m_vertexIndex;
		unsigned int m_texCoordIndex;
		unsigned int m_normalIndex;
	public:
		OBJIndex(void);
		OBJIndex(unsigned int vertexIndex, unsigned int texCoordIndex, unsigned int normalIndex);
		OBJIndex(const OBJIndex& other);
		~OBJIndex(void);

		unsigned int getVertexIndex(void) const;
		unsigned int getTexCoordIndex(void) const;
		unsigned int getNormalIndex(void) const;
		
		void setVertexIndex(unsigned int vertexIndex);
		void setTexCoordIndex(unsigned int texCoordIndex);
		void setNormalIndex(unsigned int normalIndex);
	};

}

#endif