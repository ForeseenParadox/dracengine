#ifndef D_MATH_H
#define D_MATH_H

namespace dr
{
	const double PI = 3.14159265358;

	float fastInverseSqrt(float n);
}

#endif