#ifndef VERTEX_BUFFER_H
#define VERTEX_BUFFER_H

#include "GL\glew.h"
#include "vertex3.h"

namespace dr
{
	class VertexBuffer
	{
	private:
		float* m_dataStack;
		GLuint m_handle;
		unsigned int m_position;
		unsigned int m_stackSize;
		GLenum m_drawMode;

		VertexBuffer(const dr::VertexBuffer& other);
		dr::VertexBuffer& operator=(const dr::VertexBuffer& other);

		void initBuffer();
	public:
		VertexBuffer(int stackSize);
		VertexBuffer(GLenum drawMode, int stackSize);
		~VertexBuffer(void);

		GLuint getHandle(void) const;
		unsigned int getPosition(void) const;
		unsigned int getStackSize(void) const;
		GLenum getDrawMode(void) const;
		void setDrawMode(GLenum drawMode);

		VertexBuffer& pushData(float dat);
		VertexBuffer& pushData(float dat[], int count);
		VertexBuffer& pushData(dr::Vertex3& vertex);
		VertexBuffer& popData(void);

		void bind(void) const;

		void clearData(void);
		void flushData();

		static void unbindVBO(void);
	};

}
#endif