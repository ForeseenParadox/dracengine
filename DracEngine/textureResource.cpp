#include "textureResource.h"
#include "SDL\SDL_image.h"
#include <iostream>

dr::TextureResource::TextureResource(const std::string& path):
dr::SingleResource(path)
{

}

dr::TextureResource::~TextureResource(void)
{

}

unsigned char* dr::TextureResource::getPixelData(void) const
{
	return m_pixelData;
}

unsigned int dr::TextureResource::getWidth(void) const
{
	return m_width;
}

unsigned int dr::TextureResource::getHeight(void) const
{
	return m_height;
}

void dr::TextureResource::load(void)
{
	SDL_Surface* surface = IMG_Load(getLocation().c_str());
	
	std::cout << "Image format is " << surface->format->format << std::endl;

	void* pixels = surface->pixels;

	m_width = surface->w;
	m_height = surface->h;
	
	m_pixelData = new unsigned char[m_width * m_height * 4];

	if (surface->format->format == SDL_PIXELFORMAT_ABGR8888)
	{
		unsigned char* argb8888 = static_cast<unsigned char*>(pixels);

		for (int x = 0; x < m_width; x++)
		{
			for (int y = 0; y < m_height; y++)
			{
				for (int i = 0; i < 4; i++)
					m_pixelData[(x + (m_height - y - 1) * m_width) * 4 + i] = argb8888[(x + y * m_width) * 4 + i];
			}
		}

	}

	SDL_FreeSurface(surface);

	m_loaded = true;
}

void dr::TextureResource::unload(void)
{
	
}