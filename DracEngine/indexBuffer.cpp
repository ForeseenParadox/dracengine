#include "indexBuffer.h"

dr::IndexBuffer::IndexBuffer(int size)
{
	glGenBuffers(1, &m_handle);
	m_data = new int[size];
	m_size = size;
}

dr::IndexBuffer::~IndexBuffer(void)
{
	delete m_data;
}

GLuint dr::IndexBuffer::getHandle(void) const
{
	return m_handle;
}

int* dr::IndexBuffer::getData(void) const
{
	return m_data;
}

int dr::IndexBuffer::getSize(void) const
{
	return m_size;
}

int dr::IndexBuffer::getTopOfStack(void) const
{
	return m_topOfStack;
}

void dr::IndexBuffer::pushIndex(int index)
{
	m_data[m_topOfStack++] = index;
}

void dr::IndexBuffer::pushIndicies(int* data, int indexCount)
{
	for (int i = 0; i < indexCount; i++)
		m_data[m_topOfStack++] = data[i];
}

int dr::IndexBuffer::popIndex(void)
{
	return m_data[m_topOfStack--];
}

void dr::IndexBuffer::bind(void)
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_handle);
}

void dr::IndexBuffer::clearData()
{
	m_topOfStack = 0;
}

void dr::IndexBuffer::flushData(GLenum bufferUsage)
{
	bind();
	
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(m_data), m_data, bufferUsage);
}

void dr::IndexBuffer::unbindIbo(void)
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}