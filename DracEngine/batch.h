#ifndef BATCH_H
#define BATCH_H

namespace dr
{

	class Batch
	{
	private:
		
	public:
		virtual void begin(void) = 0;
		virtual void flush(void) = 0;
		virtual void end(void) = 0;
	};
}

#endif