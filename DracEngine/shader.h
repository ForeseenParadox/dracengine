#ifndef SHADER_H
#define SHADER_H

#include <string>
#include <unordered_map>
#include "shaderResource.h"
#include "vertexAttrib.h"
#include "vec2.h"
#include "vec3.h"
#include "color.h"
#include "mat4.h"
#include "GL\glew.h"

namespace dr
{
	class ShaderProgram
	{
	private:
		GLuint m_programHandle;
		GLuint m_vertexHandle;
		GLuint m_fragmentHandle;

		int createShader(const std::string& source, int shaderType);

		static std::unordered_map<GLuint, std::unordered_map<std::string, GLint>> s_uniformLocationMemo;
		
		ShaderProgram(dr::ShaderProgram& other);
		dr::ShaderProgram& operator=(const dr::ShaderProgram& other);
	public:

		ShaderProgram(void);
		ShaderProgram(const dr::VertexAttrib attribs[], int attribCount);
		ShaderProgram(const dr::ShaderResource& resource, const dr::VertexAttrib attribs[], int attribCount);
		ShaderProgram(const std::string& vertexSource, const std::string& fragmentSource, const dr::VertexAttrib attribs[], int attribCount);
		~ShaderProgram(void);

		GLuint getProgramHandle(void) const;
		GLuint getVertexHandle(void) const;
		GLuint getFragmentHandle(void) const;
		bool isVertexShaderCreated(void) const;
		bool isFragmentShaderCreated(void) const;

		void compileAndLink(const dr::ShaderResource& resource);
		void compileAndLink(const std::string& vertexSource, const std::string& fragmentSource);
		void compileVertexShader(const std::string& vertexSource);
		void compileFragmentShader(const std::string& fragmentSource);
		void linkProgram(void);

		GLint getUniformLocation(const std::string& name) const;
		void setUniformInt(const std::string& name, int i) const;
		void setUniformFloat(const std::string& name, float f) const;
		void setUniformVector2(const std::string& name, const dr::Vec2f& vector) const;
		void setUniformVector3(const std::string& name, const dr::Vec3f& vector) const;
		void setUniformColor3(const std::string& name, const dr::Color& color) const;
		void setUniformColor4(const std::string& name, const dr::Color& color) const;
		void setUniformMatrix4f(const std::string& name, const dr::Matrix4f& mat) const;

		void bindAttribs(const dr::VertexAttrib attribs[], int attribCount);
		void bindProgram(void) const;
	};
}

#endif