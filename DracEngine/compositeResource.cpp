#include "compositeResource.h"

dr::CompositeResource::CompositeResource(void)
{

}

dr::CompositeResource::CompositeResource(Resource** children, int childrenCount)
{
	setChildrenCount(childrenCount);
	setChildren(children);
}

dr::CompositeResource::~CompositeResource(void)
{

}

dr::Resource** dr::CompositeResource::getChildren(void) const
{
	return m_children;
}

void dr::CompositeResource::setChildrenCount(int count)
{
	m_childrenCount = count;
}

void dr::CompositeResource::setChildren(Resource** children)
{
	m_children = children;
}

void dr::CompositeResource::load(void)
{
	for (int i = 0; i < m_childrenCount; i++)
	{
		m_children[i]->load();
	}
	m_loaded = true;
}

void dr::CompositeResource::unload(void)
{
	for (int i = 0; i < m_childrenCount; i++)
	{
		m_children[i]->unload();
	}
}