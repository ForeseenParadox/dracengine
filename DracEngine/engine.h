#ifndef ENGINE_H
#define ENGINE_H

#define CRASH_EXIT 0x00
#define USER_EXIT 0x01

#include "econfig.h"

union SDL_Event;

namespace dr
{

	// core forward declarations
	class Core;
	class SimulationManager;

	// subsystem forward declarations
	class SDLSys;
	class ResourceManager;
	class Window;
	class Input;

	const int SUBSYSTEM_COUNT = 4;
	const int UPDATABLE_SUBSYSTEM_COUNT = 2;
	const int RENDERABLE_SUBSYSTEM_COUNT = 1;

	class Engine
	{
	private:

		// core engine components
		dr::Core* m_core;
		dr::SimulationManager* m_simulationManager;
		dr::EngineConfig m_config;

		// subsystems
		dr::SDLSys* m_sdlSystem;
		dr::Window* m_window;
		dr::ResourceManager* m_resourceManager;
		dr::Input* m_inputSystem;

		// game
		dr::BaseGame* m_game;

		/* Singleton reference */
		static Engine* s_instance;

		static void processEvent(SDL_Event* e);
		static void loadingCompleteCallback(void);
		static void updateCallback(float delta);
		static void renderCallback(void);

		bool m_postLoad;

		void checkInstantiation(void);
		void initEngine(const dr::EngineConfig& config);
	public:
		Engine(const dr::EngineConfig& config);
		~Engine(void);

		static dr::Engine* getInstance(void);

		dr::SDLSys* getSDLSystem(void) const;
		dr::Window* getWindow(void) const;
		dr::ResourceManager* getResourceManager(void) const;
		dr::BaseGame* getGame(void) const;
		dr::Input* getInputSystem(void) const;
		dr::Core* getCore(void) const;
		dr::SimulationManager* getSimulationManager(void) const;
		dr::EngineConfig& getConfiguration(void);

		void launch(dr::BaseGame* game);
		void exit(int exitReason);
	};
}
#endif