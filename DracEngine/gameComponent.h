#ifndef GAME_COMPONENT_H
#define GAME_COMPONENT_H

#include "transform.h"

namespace dr
{

	class GameComponent
	{
	private:

	public:

		GameComponent(void);
		~GameComponent(void);

		virtual void update(float delta);
		virtual void render(const dr::Transform& transform);
	};

}

#endif