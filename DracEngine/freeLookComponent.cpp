#include "freeLookComponent.h"

#include <iostream>
#include "engine.h"
#include "input.h"
#include "SDL\SDL.h"
#include <math.h>
#include "window.h"
#include "dmath.h"

dr::FreeLookComponent::FreeLookComponent(dr::CameraComponent* camera)
{
	m_camera = camera;

	Engine::getInstance()->getWindow()->setRelativeMouseModeEnabled(true);
}

dr::FreeLookComponent::~FreeLookComponent(void)
{

}

void dr::FreeLookComponent::update(float delta)
{
	dr::Input* in = Engine::getInstance()->getInputSystem();

	dr::Vec2f r = in->getRelativeMousePosition();
	
	m_rotX += -r.getY() * 0.005f;
	m_rotY += r.getX() * 0.005f;

	if (m_rotX > dr::PI / 2)
		m_rotX = dr::PI / 2;
	if (m_rotX < -dr::PI / 2)
		m_rotX = -dr::PI / 2;

	if (in->isKeyPressed(SDL_SCANCODE_W))
	{
			float cos = std::cos(-m_rotY - dr::PI / 2) * MOVE_SPEED;
			float sin = std::sin(-m_rotY - dr::PI / 2) * MOVE_SPEED;

			m_tx += cos;
			m_tz += sin;
		}

		if (in->isKeyPressed(SDL_SCANCODE_S))
		{
			float cos = std::cos(-m_rotY - dr::PI / 2) * MOVE_SPEED;
			float sin = std::sin(-m_rotY - dr::PI / 2) * MOVE_SPEED;

			m_tx -= cos;
			m_tz -= sin;
		}

		if (in->isKeyPressed(SDL_SCANCODE_A))
		{
			float cos = std::cos(-m_rotY) * MOVE_SPEED;
			float sin = std::sin(-m_rotY) * MOVE_SPEED;

			m_tx += cos;
			m_tz += sin;
		}

		if (in->isKeyPressed(SDL_SCANCODE_D))
		{
			float cos = std::cos(-m_rotY) * MOVE_SPEED;
			float sin = std::sin(-m_rotY) * MOVE_SPEED;

			m_tx -= cos;
			m_tz -= sin;
		}

		if (in->isKeyPressed(SDL_SCANCODE_SPACE))
		{
			m_ty -= MOVE_SPEED;
		}

		if (in->isKeyPressed(SDL_SCANCODE_LSHIFT))
		{
			m_ty += MOVE_SPEED;
		}

	Transform transform;
	transform.setTranslation(m_tx, m_ty, m_tz);
	transform.setRotation(m_rotX, m_rotY, 0);
	m_camera->setCameraTransform(transform);

}