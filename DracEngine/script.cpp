#include "script.h"
#include "scriptModule.h"
#include "sutils.h"
#include <iostream>

dr::Script::Script(dr::ScriptResource* scripts, int moduleCount)
{
	m_scripts = scripts;
	m_moduleCount = moduleCount;
}

dr::Script::~Script(void)
{

}

int dr::Script::getModuleCount(void) const
{
	return m_moduleCount;
}

dr::ScriptResource* dr::Script::getScripts(void) const
{
	return m_scripts;
}

void dr::Script::compileScript(void)
{
	for (int i = 0; i < m_moduleCount; i++)
	{
		ScriptResource& resource = m_scripts[i];
		ScriptModule module;

		std::vector<std::string> srcCode = splitString(resource.getLoadedText(), '\n');

		std::string functionName;
		std::vector<std::string> functionLines;
		for (std::vector<std::string>::iterator itr = srcCode.begin(); itr != srcCode.end(); itr++)
		{
			std::string& line = *itr;

			// tokens delim by space and trimmed
			std::vector<std::string> tokens = splitString(line, ' ', true);

			if (tokens[0] == "module")
				module.setName(tokens[1]);
			else if (tokens[0] == "function")
				functionName = tokens[1];
			else if (tokens[0] == "end")
			{
				dr::ScriptFunction function; 
				compileScriptFunction(functionLines, function);
				module.registerFunction(functionName, function);
			}
			else
				functionLines.push_back(line);
		}
	}

}

void dr::Script::compileScriptFunction(std::vector<std::string>& srcCode, dr::ScriptFunction& dst)
{
	std::vector<unsigned int> byteCode;
	for (std::vector<std::string>::iterator itr = srcCode.begin(); itr != srcCode.end(); itr++)
	{
		std::string& line = *itr;
		std::vector<std::string> tokens = splitString(line, ' ', true);
		
		if (tokens.at(0) == "push")
		{
			byteCode.push_back(0x04);
		}
		else if (tokens.at(0) == "pop")
		{
			
		}
	}
	// set function byte code data
	dst.setByteCodeLength(byteCode.size());
	unsigned int* byteCodeAsArray = new unsigned int[byteCode.size()];
	for (int i = 0; i < byteCode.size(); i++)
		byteCodeAsArray[i] = byteCode[i];
	dst.setByteCode(byteCodeAsArray);

	srcCode.clear();
}

unsigned int* dr::Script::compileExpression(std::string& expression)
{
	std::vector<std::string> tokens = splitString(expression, ' ');
	return 0;
}