#include "resource.h"

dr::Resource::Resource(void)
{
	m_loaded = false;
}

dr::Resource::~Resource(void)
{

}

bool dr::Resource::isLoaded(void) const
{
	return m_loaded;
}