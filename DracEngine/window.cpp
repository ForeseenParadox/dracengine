#include "window.h"
#include "SDL\SDL_video.h"
#include "SDL\SDL.h"
#include "econfig.h"
#include "engine.h"
#include "GL\glew.h"
#include <iostream>

dr::Window::Window(const dr::EngineConfig& config) :
m_defaultWindowInfo(config.getWindowInfo()),
Subsystem(config,false,true)
{
	// do nothing
}

dr::Window::~Window(void)
{

}

int dr::Window::getWidth(void) const
{
	int w = 0;
	SDL_GetWindowSize(m_window, &w, NULL);
	return w;
}

int dr::Window::getHeight(void) const
{
	int h = 0;
	SDL_GetWindowSize(m_window, NULL, &h);
	return h;
}

void dr::Window::setRelativeMouseModeEnabled(bool enabled)
{
	if (enabled)
		SDL_SetRelativeMouseMode(SDL_TRUE);
	else
		SDL_SetRelativeMouseMode(SDL_FALSE);
}

void dr::Window::setMousePosition(int x, int y)
{
	// don't classify the warp as a motion event
	SDL_WarpMouseInWindow(m_window, x, y);
}

void dr::Window::init(void)
{
	m_window = SDL_CreateWindow(m_defaultWindowInfo.getTitle().c_str(), m_defaultWindowInfo.getPositionX(), m_defaultWindowInfo.getPositionY(), m_defaultWindowInfo.getWidth(), m_defaultWindowInfo.getHeight(), SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
	m_context = SDL_GL_CreateContext(m_window);
	
	GLuint result = glewInit();
	if (result != GLEW_OK)
	{
		std::string errMsg = "GLEW returned error code " + result;
		std::cerr << errMsg << std::endl;
		throw std::runtime_error(errMsg);
	} else
	{
		std::cout << "GLEW initialized successfully." << std::endl;
	}
}

void dr::Window::preRender(void)
{
	SDL_GL_SwapWindow(m_window);
}

void dr::Window::render(void)
{
	SDL_GL_SwapWindow(m_window);
}

void dr::Window::dispose(void)
{
	SDL_GL_DeleteContext(m_context);
	SDL_DestroyWindow(m_window);
}