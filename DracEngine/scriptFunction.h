#ifndef SCRIPT_FUNCTION_H
#define SCRIPT_FUNCTION_H

namespace dr
{

	class ScriptFunction
	{
	private:
		int m_byteCodeLength;
		unsigned int* m_byteCode;
	public:

		ScriptFunction(void);
		ScriptFunction(int byteCodeLength, unsigned int* byteCode);
		~ScriptFunction(void);

		int getByteCodeLength(void) const;
		unsigned int* getByteCode(void);
		
		void setByteCodeLength(int byteCodeLength);
		void setByteCode(unsigned int* byteCode);
	};
}

#endif