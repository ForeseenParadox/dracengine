#include "frameBuffer.h"
#include "engine.h"
#include "window.h"
#include <string>
#include <sstream>
#include <iostream>

const int dr::FrameBuffer::S_DEFAULT_DRAWBUFFER_COUNT = 1;
GLenum dr::FrameBuffer::S_DEFAULT_DRAWBUFFERS[S_DEFAULT_DRAWBUFFER_COUNT] = {GL_COLOR_ATTACHMENT0};

dr::FrameBuffer::FrameBuffer():
m_colorAttachment(&m_defaultColorAttachment)
{
	glGenFramebuffers(1, &m_handle);

	initDefaultColorAttachment();
	attachTexture(m_colorAttachment, GL_COLOR_ATTACHMENT0);

	setDrawBuffers(S_DEFAULT_DRAWBUFFERS, S_DEFAULT_DRAWBUFFER_COUNT);
}

dr::FrameBuffer::FrameBuffer(bool colorBuffer, bool depthBuffer, bool stencilBuffer) :
m_colorAttachment(&m_defaultColorAttachment)
{
	glGenFramebuffers(1, &m_handle);

	initDefaultColorAttachment();

	if (colorBuffer)
		attachTexture(m_colorAttachment, GL_COLOR_ATTACHMENT0);

	if (depthBuffer)
	{

	}

	if (stencilBuffer)
	{

	}

	setDrawBuffers(S_DEFAULT_DRAWBUFFERS, S_DEFAULT_DRAWBUFFER_COUNT);
}

dr::FrameBuffer::~FrameBuffer()
{
	glDeleteFramebuffers(1, &m_handle);
}

void dr::FrameBuffer::initDefaultColorAttachment(void)
{
	dr::Window* wnd = Engine::getInstance()->getWindow();
	m_defaultColorAttachment.loadEmptyTexture(wnd->getWidth(), wnd->getHeight(), GL_NEAREST);
}

GLuint dr::FrameBuffer::getHandle(void) const
{
	return m_handle;
}

const dr::Texture* dr::FrameBuffer::getColorAttachment(void) const
{
	return m_colorAttachment;
}

void dr::FrameBuffer::setDrawBuffers(GLenum drawBuffers[], int count)
{
	bind();
	glDrawBuffers(count, drawBuffers);
}

void dr::FrameBuffer::attachTexture(const dr::Texture* texture, GLenum attachment)
{
	bind();

	glFramebufferTexture2D(GL_FRAMEBUFFER, attachment, GL_TEXTURE_2D, texture->getHandle(), 0);

	if (attachment == GL_COLOR_ATTACHMENT0)
		m_colorAttachment = texture;
}

void dr::FrameBuffer::checkComplete(void)
{
	bind();
	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if (status != GL_FRAMEBUFFER_COMPLETE)
	{
		std::stringstream stream;
		stream << "FBO returned incomplete status code of " << status;
		std::cerr << stream.str() << std::endl;
		throw std::runtime_error(stream.str());
	}
}

void dr::FrameBuffer::bind(void)
{
	glBindFramebuffer(GL_FRAMEBUFFER, m_handle);
}		

void dr::FrameBuffer::bindDefaultFrameBuffer()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}
