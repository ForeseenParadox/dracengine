#include "windowInfo.h"
#include "SDL\SDL_video.h"

dr::WindowInfo::WindowInfo(void):
m_title("Game"),
m_width(16 * 60),
m_height(9 * 60),
m_positionX(SDL_WINDOWPOS_CENTERED),
m_positionY(SDL_WINDOWPOS_CENTERED)
{
}

dr::WindowInfo::WindowInfo(const std::string& title, int width, int height, int positionX, int positionY):
m_title(title),
m_width(width),
m_height(height),
m_positionX(positionX),
m_positionY(positionY)
{
}

dr::WindowInfo::WindowInfo(const WindowInfo& other):
m_title(other.m_title),
m_width(other.m_width),
m_height(other.m_height),
m_positionX(other.m_positionX),
m_positionY(other.m_positionY)
{
}

dr::WindowInfo::~WindowInfo(void)
{
}

std::string dr::WindowInfo::getTitle(void) const
{
	return m_title;
}

int dr::WindowInfo::getWidth(void) const
{
	return m_width;
}

int dr::WindowInfo::getHeight(void) const
{
	return m_height;
}

int dr::WindowInfo::getPositionX(void) const
{
	return m_positionX;
}

int dr::WindowInfo::getPositionY(void) const
{
	return m_positionY;
}

void dr::WindowInfo::setTitle(const std::string& title)
{
	m_title = title;
}

void dr::WindowInfo::setWidth(int width)
{
	m_width = width;
}

void dr::WindowInfo::setHeight(int height)
{
	m_height = height;
}

void dr::WindowInfo::setPositionX(int positionX)
{
	m_positionX = positionX;
}

void dr::WindowInfo::setPositionY(int positionY)
{
	m_positionY = positionY;
}

dr::WindowInfo& dr::WindowInfo::operator=(const dr::WindowInfo& other)
{
	m_title = other.m_title;
	m_width = other.m_width;
	m_height = other.m_height;
	m_positionX = other.m_positionX;
	m_positionY = other.m_positionY;
	return *this;
}