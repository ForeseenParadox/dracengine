#ifndef GAME_OBJECT_H
#define GAME_OBJECT_H

#include <vector>
#include "transform.h"

namespace dr
{

	class GameComponent;

	class GameObject
	{
	private:
		dr::Transform m_transform;
		dr::GameObject* m_parent;
		std::vector<dr::GameObject*> m_children;
		std::vector<dr::GameComponent*> m_components;
	public:
		GameObject(void);
		GameObject(dr::GameObject* parent);
		~GameObject(void);

		dr::Transform getTransform(void) const;
		dr::GameObject* getParent(void);
		std::vector<dr::GameObject*> getChildren(void);
		std::vector<dr::GameComponent*> getComponents(void) const;
		int getComponentCount(void) const;
		int getChildrenCount(void) const;
		dr::GameObject* getChild(int index);
		dr::GameComponent* getComponent(int index);

		void setTransform(const dr::Transform& transform);
		void setParent(dr::GameObject* parent);

		void update(float delta);
		void render(void);
		void updateAll(float delta);
		void renderAll(void);

		void attachComponent(dr::GameComponent* component);
		void detachComponent(int componentIndex);

		void attachChild(dr::GameObject* child);
		void detachChild(int childIndex);
	};
}

#endif