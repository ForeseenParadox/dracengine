#ifndef MAT_4_H
#define MAT_4_H

#include "vec2.h"

namespace dr
{
	class Matrix4f
	{

	private:
		float* m_data;
	public:
		Matrix4f();
		Matrix4f(const dr::Matrix4f& copy);
		Matrix4f(dr::Matrix4f&& rv);
		~Matrix4f(void);

		float* getData(void) const;
		float getElement(int r, int c) const;
		void setElement(int r, int c, float v);

		Matrix4f add(const dr::Matrix4f& other) const;
		Matrix4f subtract(const dr::Matrix4f& other) const;
		Matrix4f multiply(const dr::Matrix4f& other) const;
		Matrix4f scale(float scaler) const;

		Matrix4f* mutateAdd(const dr::Matrix4f& other);
		Matrix4f* mutateSubtract(const dr::Matrix4f& other);
		Matrix4f* mutateMultiply(const dr::Matrix4f& other);

		Matrix4f* mutateMultiply(float* x, float* y);
		Matrix4f* mutateMultiply(float* x, float* y, float* z);
		Matrix4f* mutateMultiply(float* x, float* y, float* z, float* w);
		Matrix4f* mutateMultiply(dr::Vec2f& other);
		
		Matrix4f* mutateScale(float scaler);

		Matrix4f& initIdentity(void);
		Matrix4f& initZero(void);
		Matrix4f& initOrthographicProjection(float left, float right, float top, float bottom, float zNear, float zFar);
		Matrix4f& initPerspectiveProjection(float fov, float aspect, float zNear, float zFar);
		Matrix4f& initTranslation(float x, float y);
		Matrix4f& initTranslation(float x, float y, float z);
		Matrix4f& initRotationX(float theta);
		Matrix4f& initRotationY(float theta);
		Matrix4f& initRotationZ(float theta);
		Matrix4f& initScale(float sx, float sy);
		Matrix4f& initScale(float sx, float sy, float sz);

		const float operator[](int index) const;
		Matrix4f operator+(const dr::Matrix4f& other) const;
		Matrix4f operator-(const dr::Matrix4f& other) const;
		Matrix4f operator*(const dr::Matrix4f& other) const;
		Matrix4f operator*(float scaler) const;
		Matrix4f operator=(const dr::Matrix4f& other);

	};
}

#endif