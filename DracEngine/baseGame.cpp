#include "baseGame.h"

#include "engine.h"
#include <iostream>

dr::BaseGame::BaseGame(dr::Engine* engine)
{
	m_engine = engine;
}

dr::BaseGame::~BaseGame(void)
{
	// do nothing
}

void dr::BaseGame::resized(void)
{

}

void dr::BaseGame::preUpdate(float delta)
{

}

void dr::BaseGame::preRender(void)
{

}

void dr::BaseGame::postLoad(void)
{

}

dr::Engine* dr::BaseGame::getEngine(void) const
{
	return m_engine;
}