#include <iostream>
#include "engine.h"
#include "vertexAttrib.h"
#include "baseGame.h"
#include "engine.h"
#include "resourceManager.h"

// graphics
#include "GL\glew.h"
#include "SDL\SDL.h"

#include "input.h"

#include "vertexBuffer.h"
#include "vertexArray.h"
#include "spriteBatch.h"
#include "texture.h"
#include "textureResource.h"
#include "textureRegion.h"
#include "frameBuffer.h"
#include "shapeBatch.h"
#include "deferredPipeline2d.h"

class Game : public dr::BaseGame
{

private:
	dr::Texture m_texture;
	dr::TextureResource m_resource;
	dr::DeferredPipeline2D m_pipeline;
	dr::PointLight m_point;
	dr::PointLight m_point2;
	float m_rotation;
public:

	Game(dr::Engine* e) :
	m_resource("Ground_Tiles.png"),
	dr::BaseGame(e)
	{
	}

	void load()
	{
		getEngine()->getResourceManager()->insertResource("Ground Tiles", &m_resource);

		getEngine()->getResourceManager()->setMultiThreadLoad(true);
	}

	void postLoad()
	{
		m_texture.load(m_resource);
		m_pipeline.getAmbientLight().m_base.m_intensity = 0.5f;

		m_point.m_base.m_color = dr::Color(1.0f, 0.0f, 0.0f);
		m_point.m_base.m_intensity = 1.0f;
		m_point.m_position = dr::Vec2f(100.0f, 100.0f);
		m_point.m_attenuation = dr::Vec3f(0.0001f, 0.0f, 2.0f);
	
		m_point2.m_base.m_color = dr::Color(0.0f, 1.0f, 0.0f);
		m_point2.m_base.m_intensity = 1.0f;
		m_point2.m_position = dr::Vec2f(400, 400);
		m_point2.m_attenuation = dr::Vec3f(0.0001f, 0.0f, 2.0f);
	}

	void update(float delta)
	{
		m_point.m_position.setX(dr::Engine::getInstance()->getInputSystem()->getMouseX());
		m_point.m_position.setY(dr::Engine::getInstance()->getInputSystem()->getMouseY());		
	}

	void render()
	{

		m_pipeline.beginPass(dr::PASS_COLOR);

		m_pipeline.getSpriteBatch().begin();
		m_pipeline.getSpriteBatch().render(&m_texture, 10, 10);
		m_pipeline.getSpriteBatch().end();
		
		m_pipeline.endPass();

		m_pipeline.renderAmbientLightPass();
		m_pipeline.renderPointLightPass(m_point);
		m_pipeline.renderPointLightPass(m_point2);
		
		// check for opengl errors
		GLenum error = glGetError();
		if (error != GL_NO_ERROR)
		{
			std::cerr << "GL Error: " << error << std::endl;
			abort();
		}
	} 

	void dispose()
	{
	}
};

int main(int argc, char** argv)
{
	
	dr::EngineConfig conf;
	conf.setMaxFps(-1);
	dr::Engine e(conf);

	Game g(&e);
	e.launch(&g);

	return 0;
}