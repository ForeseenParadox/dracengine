#include <iostream>
#include "core.h"
#include "subsystem.h"
#include "GL\glew.h"

dr::Core::Core(int subsystemCount, int updatableSubsystemCount, int renderableSubsystemCount)
{
	m_subsystemCount = subsystemCount;
	m_updatableSubsystemCount = updatableSubsystemCount;
	m_renderableSubsystemCount = renderableSubsystemCount;

	m_initializeQueue = new Subsystem*[m_subsystemCount];
	m_updateQueue = new Subsystem*[m_updatableSubsystemCount];
	m_renderQueue = new Subsystem*[m_renderableSubsystemCount];
}

dr::Core::~Core(void)
{
	delete[] m_initializeQueue;
	delete[] m_updateQueue;
	delete[] m_renderQueue;
}

void dr::Core::insertSubsystem(int priority, dr::Subsystem* subsystem)
{
	m_initializeQueue[priority] = subsystem;
}

void dr::Core::insertUpdatableSubsystem(int priority, dr::Subsystem* subsystem)
{
	m_updateQueue[priority] = subsystem;
}

void dr::Core::insertRenderableSubsystem(int priority, dr::Subsystem* subsystem)
{
	m_renderQueue[priority] = subsystem;
}

void dr::Core::initSubsystems(void)
{
	for (int i = 0; i < m_subsystemCount; i++)
	{
		if (m_initializeQueue[i] != nullptr)
			m_initializeQueue[i]->init();
	}
}

void dr::Core::updateSubsystems(float delta, bool preUpdate)
{
	for (int i = 0; i < m_updatableSubsystemCount; i++)
	{
		if (m_updateQueue[i] != nullptr && m_updateQueue[i]->isUpdatable())
		{
			if (!preUpdate)
				m_updateQueue[i]->update(delta);
			else
				m_updateQueue[i]->preUpdate(delta);
		}
	}
}

void dr::Core::renderSubsystems(bool preRender)
{
	for (int i = 0; i < m_renderableSubsystemCount; i++)
	{
		if (m_renderQueue[i] != nullptr && m_renderQueue[i]->isRenderable())
		{
			if (!preRender)
				m_renderQueue[i]->render();
			else
				m_renderQueue[i]->preRender();
		}
	}
}

void dr::Core::disposeSubsystems(void)
{
	for (int i = m_subsystemCount - 1; i >= 0; i--)
	{
		if (m_initializeQueue[i] != nullptr)
			m_initializeQueue[i]->dispose();
	}
}