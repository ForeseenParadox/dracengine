#include "resource.h"
#include "resourceManager.h"
#include "SDL\SDL_thread.h"
#include <iostream>

dr::ResourceManager::ResourceManager(const dr::EngineConfig& config, void(*loadCallback)(void)) :
m_multiThreadLoad(false),
m_loadCallback(loadCallback),
Subsystem(config, false, false)
{
}

dr::ResourceManager::~ResourceManager(void)
{
	m_multiThreadLoad = false;
}

std::unordered_map<std::string, dr::Resource*> dr::ResourceManager::getResourceMap(void) const
{
	return m_resourceMap;
}

bool dr::ResourceManager::isLoading(void) const
{
	return m_loading;
}

bool dr::ResourceManager::isMultiThreadLoad(void) const
{
	return m_multiThreadLoad;
}

void dr::ResourceManager::setMultiThreadLoad(bool multiThreadLoad)
{
	m_multiThreadLoad = multiThreadLoad;
}

void dr::ResourceManager::init(void)
{

}

void dr::ResourceManager::dispose(void)
{

}

bool dr::ResourceManager::insertResource(const std::string& name, dr::Resource* res)
{
	Resource* existing = m_resourceMap[name];

	if (existing != nullptr)
		return false;

	m_resourceMap[name] = res;

	std::cout << m_resourceMap.size() << " resource count" << std::endl;

	return true;
}

void dr::ResourceManager::removeResource(const std::string& name)
{
	m_resourceMap.erase(name);
}

void dr::ResourceManager::loadAllResources(void)
{
	m_loading = true;

	if (m_multiThreadLoad)
		SDL_CreateThread(&loadAllResourcesFunction, "Loading Thread", this);
	else
		loadAllResourcesFunction(this);
}

void dr::ResourceManager::unloadAllResources(void)
{
	unloadAllResourcesFunction(this);
}

int dr::ResourceManager::loadAllResourcesFunction(void* data)
{

	ResourceManager* resMap = static_cast<ResourceManager*>(data);

	std::unordered_map<std::string, Resource*>& map = resMap->getResourceMap();
	std::unordered_map<std::string, Resource*>::iterator itr;

	for (itr = map.begin(); itr != map.end(); itr++)
	{

		dr::Resource* resource = itr->second;

		if (!resource->isLoaded())
		{
			resource->load();
		}
	}

	resMap->m_loadCallback();

	resMap->m_loading = false;

	return 0;
}

int dr::ResourceManager::unloadAllResourcesFunction(void* data)
{
	ResourceManager* resMap = static_cast<ResourceManager*>(data);

	std::unordered_map<std::string, Resource*>& map = resMap->getResourceMap();
	std::unordered_map<std::string, Resource*>::iterator itr;

	for (itr = map.begin(); itr != map.end(); itr++)
	{
		dr::Resource* resource = itr->second;
		if (resource->isLoaded())
			resource->unload();
	}

	return 1;
}

dr::Resource* dr::ResourceManager::getResource(const std::string& name)
{
	return m_resourceMap[name];
}