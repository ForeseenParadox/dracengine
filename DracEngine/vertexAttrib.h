#ifndef VERTEX_ATTRIB_H
#define VERTEX_ATTRIB_H
#include <string>

namespace dr
{

	const std::string POSITION_ATTRIB_NAME = "a_position";
	const std::string COLOR_ATTRIB_NAME = "a_color";
	const std::string TEXCOORD_ATTRIB_NAME = "a_texCoord";
	const std::string NORMAL_ATTRIB_NAME = "a_normal";

	class VertexAttrib
	{
	private:
		int m_index;
		int m_size;
		std::string m_name;
	public:
		
		VertexAttrib(void);
		VertexAttrib(const dr::VertexAttrib& other);
		VertexAttrib(int index, int size, const std::string& name);

		int getIndex(void) const;
		int getSize(void) const;
		std::string getName(void) const;

		void setIndex(int index);
		void setSize(int size);
		void setName(const std::string& name);

		VertexAttrib& operator=(const dr::VertexAttrib& other);
	};
}

#endif