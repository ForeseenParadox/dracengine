#include "vertexAttrib.h"

dr::VertexAttrib::VertexAttrib(void)
{

}

dr::VertexAttrib::VertexAttrib(const dr::VertexAttrib& other)
{
	m_name = other.m_name;
	m_index = other.m_index;
	m_size = other.m_size;
}

dr::VertexAttrib::VertexAttrib(int index, int size, const std::string& name)
{
	m_index = index;
	m_size = size;
	m_name = name;
}

int dr::VertexAttrib::getIndex(void) const
{
	return m_index;
}

int dr::VertexAttrib::getSize(void) const
{
	return m_size;
}

std::string dr::VertexAttrib::getName(void) const
{
	return m_name;
}

void dr::VertexAttrib::setIndex(int index)
{
	m_index = index;
}

void dr::VertexAttrib::setSize(int size)
{
	m_size = size;
}

void dr::VertexAttrib::setName(const std::string& name)
{
	m_name = name;
}

dr::VertexAttrib& dr::VertexAttrib::operator=(const dr::VertexAttrib& other)
{
	m_name = other.m_name;
	m_index = other.m_index;
	m_size = other.m_size;
	return *this;
}