#include "objIndex.h"
#include <iostream>

dr::OBJIndex::OBJIndex(void):
m_vertexIndex(0),
m_texCoordIndex(0),
m_normalIndex(0)
{
}

dr::OBJIndex::OBJIndex(const OBJIndex& other):
m_vertexIndex(other.getVertexIndex()),
m_texCoordIndex(other.getTexCoordIndex()),
m_normalIndex(other.getNormalIndex())
{

}

dr::OBJIndex::OBJIndex(unsigned int vertexIndex, unsigned int texCoordIndex, unsigned int normalIndex) :
m_vertexIndex(vertexIndex),
m_texCoordIndex(texCoordIndex),
m_normalIndex(normalIndex)
{
}


dr::OBJIndex::~OBJIndex(void)
{

}

unsigned int dr::OBJIndex::getVertexIndex(void) const
{
	return m_vertexIndex;
}

unsigned int dr::OBJIndex::getTexCoordIndex(void) const
{
	return m_texCoordIndex;
}

unsigned int dr::OBJIndex::getNormalIndex(void) const
{
	return m_normalIndex;
}

void dr::OBJIndex::setVertexIndex(unsigned int vertexIndex)
{
	m_vertexIndex = vertexIndex;
}

void dr::OBJIndex::setTexCoordIndex(unsigned int texCoordIndex)
{
	m_texCoordIndex = texCoordIndex;
}

void dr::OBJIndex::setNormalIndex(unsigned int normalIndex)
{
	m_normalIndex = normalIndex;
}
