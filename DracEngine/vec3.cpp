#include "vec3.h"
#include <math.h>
#include "dmath.h"
#include <iostream>

dr::Vec3f::Vec3f(void)
{
	m_x = 0;
	m_y = 0;
	m_z = 0;
}

dr::Vec3f::Vec3f(float x, float y, float z)
{
	m_x = x;
	m_y = y;
	m_z = z;
}

dr::Vec3f::Vec3f(const dr::Vec3f& other)
{
	m_x = other.m_x;
	m_y = other.m_y;
	m_z = other.m_z;
}

dr::Vec3f::Vec3f(dr::Vec3f&& other)
{
	m_x = other.m_x;
	m_y = other.m_y;
	m_z = other.m_z;
}

dr::Vec3f::~Vec3f(void)
{
	// do nothing
}

float dr::Vec3f::getX(void) const
{
	return m_x;
}

float dr::Vec3f::getY(void) const
{
	return m_y;
}

float dr::Vec3f::getZ(void) const
{
	return m_z;
}

void dr::Vec3f::setX(float x)
{
	m_x = x;
}

void dr::Vec3f::setY(float y)
{
	m_y = y;
}

void dr::Vec3f::setZ(float z)
{
	m_z = z;
}

float dr::Vec3f::length(void) const
{
	return static_cast<float>(sqrt(m_x * m_x + m_y * m_y + m_z * m_z));
}

float dr::Vec3f::distance(const dr::Vec3f& other) const
{
	return subtract(other).length();
}

float dr::Vec3f::dot(const dr::Vec3f& other) const
{
	return m_x * other.m_x + m_y * other.m_y + m_z * other.m_z;
}

void dr::Vec3f::initZero(void)
{
	m_x = 0;
	m_y = 0;
	m_z = 0;
}

dr::Vec3f* dr::Vec3f::mutateAdd(const dr::Vec3f& other)
{
	m_x += other.m_x;
	m_y += other.m_y;
	m_z += other.m_z;

	return this;
}

dr::Vec3f* dr::Vec3f::mutateAdd(float x, float y, float z)
{
	m_x += x;
	m_y += y;
	m_z += z;

	return this;
}


dr::Vec3f* dr::Vec3f::mutateSubtract(const dr::Vec3f& other)
{
	m_x -= other.m_x;
	m_y -= other.m_y;
	m_z -= other.m_z;

	return this;
}

dr::Vec3f* dr::Vec3f::mutateSubtract(float x, float y, float z)
{
	m_x -= x;
	m_y -= y;
	m_z -= z;

	return this;
}

dr::Vec3f* dr::Vec3f::mutateScale(float scaler)
{
	m_x *= scaler;
	m_y *= scaler;
	m_z *= scaler;

	return this;
}

dr::Vec3f* dr::Vec3f::mutateScale(float scaleX, float scaleY, float scaleZ)
{
	m_x *= scaleX;
	m_y *= scaleY;
	m_z *= scaleZ;

	return this;
}

dr::Vec3f* dr::Vec3f::mutateScale(const dr::Vec3f& other)
{
	m_x *= other.m_x;
	m_y *= other.m_y;
	m_z *= other.m_z;

	return this;
}

dr::Vec3f* dr::Vec3f::mutateNormalize(void)
{
#ifdef DR_FAST_MATH

	float l = fastInverseSqrt(m_x * m_x + m_y * m_y);

	m_x *= l;
	m_y *= l;

#else

	float l = length();

	m_x /= l;
	m_y /= l;
#endif

	return this;
}

dr::Vec3f dr::Vec3f::add(const dr::Vec3f& other) const
{
	return Vec3f(m_x + other.m_x, m_y + other.m_y, m_z + other.m_z);
}

dr::Vec3f dr::Vec3f::add(float x, float y, float z) const
{
	return Vec3f(m_x + x, m_y + y, m_z + z);
}

dr::Vec3f dr::Vec3f::subtract(const dr::Vec3f& other) const
{
	return Vec3f(m_x - other.m_x, m_y - other.m_y, m_z + other.m_z);
}

dr::Vec3f dr::Vec3f::subtract(float x, float y, float z) const
{
	return Vec3f(m_x - x, m_y - y, m_z - z);
}

dr::Vec3f dr::Vec3f::scale(float scaler) const
{
	return Vec3f(m_x * scaler, m_y * scaler, m_z * scaler);
}

dr::Vec3f dr::Vec3f::scale(float scaleX, float scaleY, float scaleZ) const
{
	return Vec3f(m_x * scaleX, m_y * scaleY, m_z * scaleZ);
}

dr::Vec3f dr::Vec3f::scale(const dr::Vec3f& other) const
{
	return Vec3f(m_x * other.m_x, m_y * other.m_y, m_z * other.m_z);
}

dr::Vec3f dr::Vec3f::normalize(void)
{

	float l = 0;
#ifdef DR_FAST_MATH

	l = fastInverseSqrt(m_x * m_x + m_y * m_y);
#else

	l = length();
#endif

	return Vec3f(m_x / l, m_y / l, m_z / l);
}

dr::Vec3f dr::Vec3f::operator+(const Vec3f& other)
{
	return add(other);
}

dr::Vec3f dr::Vec3f::operator-(const Vec3f& other)
{
	return subtract(other);
}

dr::Vec3f dr::Vec3f::operator*(const Vec3f& other)
{
	return scale(other);
}

dr::Vec3f dr::Vec3f::operator*(float scaler)
{
	return scale(scaler);
}

dr::Vec3f dr::Vec3f::operator+=(const Vec3f& other)
{
	return *mutateAdd(other);
}

dr::Vec3f dr::Vec3f::operator-=(const Vec3f& other)
{
	return *mutateSubtract(other);
}

dr::Vec3f dr::Vec3f::operator*=(const Vec3f& other)
{
	return *mutateScale(other);
}

dr::Vec3f dr::Vec3f::operator*=(float scaler)
{
	return *mutateScale(scaler);
}


bool dr::Vec3f::operator==(const dr::Vec3f& other) const
{
	if (m_x == other.m_x && m_y == other.m_y && m_z == other.m_z)
		return true;
	else
		return false;
}