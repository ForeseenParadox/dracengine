#ifndef DEFERRED_PIPELINE_H_2D
#define DEFERRED_PIPELINE_H_2D

#include "frameBuffer.h"
#include "lights2d.h"
#include "spriteBatch.h"
#include "vertexBuffer.h"
#include "vertexArray.h"
#include "vertexAttrib.h"

namespace dr
{

	const unsigned int PASS_DISABLED = 0x00;
	const unsigned int PASS_COLOR = 0x01;
	const unsigned int PASS_NORMAL = 0x02;
	const unsigned int PASS_DEPTH = 0x03;

	const std::string UNIFORM_AMBIENT_INTENSITY = "u_ambientIntensity";
	const std::string UNIFORM_AMBIENT_COLOR = "u_ambientColor";

	struct RenderPass
	{
		dr::VertexBuffer m_vbo;
		dr::VertexArray m_vao;
		dr::ShaderProgram m_shader;

		RenderPass(GLenum drawMode, int vboSize);
	};

	class DeferredPipeline2D
	{
	private:

		static const unsigned int S_AMBIENT_VERTEX_ATTRIB_COUNT;
		static const dr::VertexAttrib S_AMBIENT_VERTEX_ATTRIBS[];
		static const unsigned int S_POINT_VERTEX_ATTRIB_COUNT;
		static const dr::VertexAttrib S_POINT_VERTEX_ATTRIBS[];
		
		// g buffer
		dr::FrameBuffer m_colorBuffer;
		unsigned int m_renderPass;

		// rendering utils
		dr::SpriteBatch m_spriteBatch;
		
		// lighting
		dr::AmbientLight m_ambientLight;

		dr::RenderPass m_ambientPass;
		dr::RenderPass m_pointPass;

		// util functions
		void initAmbientPass();
		void initPointPass();
		void enableFBO();
		void disableFBO();
	public:
		DeferredPipeline2D(void);
		~DeferredPipeline2D(void);

		const dr::FrameBuffer& getColorBuffer(void) const;
		unsigned int getRenderPass(void) const;
		dr::SpriteBatch& getSpriteBatch(void);
		dr::AmbientLight& getAmbientLight(void);

		void beginPass(unsigned int renderPass);
		void endPass(void);

		void renderAmbientLightPass(void);
		void renderPointLightPass(const dr::PointLight& pointLight);
	};

}

#endif