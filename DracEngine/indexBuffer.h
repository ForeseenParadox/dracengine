#ifndef INDEX_BUFFER_H
#define INDEX_BUFFER_H

#include "GL\glew.h"

namespace dr
{
	class IndexBuffer
	{
	private:
		GLuint m_handle;
		int* m_data;
		int m_size;
		int m_topOfStack;
	public:
		IndexBuffer(int size);
		~IndexBuffer(void);

		GLuint getHandle(void) const;
		int* getData(void) const;
		int getSize(void) const;
		int getTopOfStack(void) const;

		void pushIndex(int index);
		void pushIndicies(int* data, int indexCount);
		int popIndex(void);
		void bind(void);
		void clearData(void);

		void flushData(GLenum bufferUsage);

		static void unbindIbo(void);
	};
}

#endif