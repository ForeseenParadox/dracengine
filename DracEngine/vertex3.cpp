#include "vertex3.h"

dr::Vertex3::Vertex3(void)
{

}

dr::Vertex3::Vertex3(const dr::Vec3f& position):
m_position(position)
{
}

dr::Vertex3::Vertex3(const dr::Vec3f& position, const dr::Color& color):
m_position(position),
m_color(color)
{
}

dr::Vertex3::Vertex3(const dr::Vec3f& position, const dr::Color& color, const dr::Vec2f& texCoord):
m_position(position),
m_color(color),
m_texCoord(texCoord)
{
}

dr::Vertex3::Vertex3(const dr::Vec3f& position, const dr::Color& color, const dr::Vec2f& texCoord, const dr::Vec3f& normal):
m_position(position),
m_color(color),
m_texCoord(texCoord),
m_normal(normal)
{
}

dr::Vertex3::Vertex3(const dr::Vertex3& other):
m_position(other.getPosition()),
m_color(other.getColor()),
m_texCoord(other.getTexCoord()),
m_normal(other.getNormal())
{
}

dr::Vec3f dr::Vertex3::getPosition(void) const
{
	return m_position;
}

dr::Color dr::Vertex3::getColor(void) const
{
	return m_color;
}

dr::Vec2f dr::Vertex3::getTexCoord(void) const
{
	return m_texCoord;
}

dr::Vec3f dr::Vertex3::getNormal(void) const
{
	return m_normal;
}

void dr::Vertex3::setPosition(const dr::Vec3f& position)
{
	m_position = position;
}

void dr::Vertex3::setColor(const dr::Color& color)
{
	m_color = color;
}

void dr::Vertex3::setTexCoord(const dr::Vec2f& texCoord)
{
	m_texCoord = texCoord;
}

void dr::Vertex3::setNormal(const dr::Vec3f& normal)
{
	m_normal = normal;
}

bool dr::Vertex3::operator==(dr::Vertex3& other) const
{
	if (other.getPosition() == m_position && other.getColor() == m_color && other.getTexCoord() == m_texCoord && other.getNormal() == m_normal)
		return true;
	else
		return false;
}