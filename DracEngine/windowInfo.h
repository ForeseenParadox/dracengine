#ifndef WINDOW_INFO_H
#define WINDOW_INFO_H

#include <string>

namespace dr
{
	class WindowInfo
	{
	private:
		std::string m_title;
		int m_width;
		int m_height;
		int m_positionX;
		int m_positionY;
	public:
		WindowInfo(void);
		WindowInfo(const std::string& title, int width, int height, int positionX, int positionY);
		WindowInfo(const WindowInfo& other);
		~WindowInfo(void);

		std::string getTitle(void) const;
		int getWidth(void) const;
		int getHeight(void) const;
		int getPositionX(void) const;
		int getPositionY(void) const;

		void setTitle(const std::string& title);
		void setWidth(int width);
		void setHeight(int height);
		void setPositionX(int positionX);
		void setPositionY(int positionY);

		WindowInfo& operator=(const WindowInfo& other);
	};
}

#endif
