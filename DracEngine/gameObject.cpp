#include "gameObject.h"
#include "gameComponent.h"
#include <iostream>

dr::GameObject::GameObject(void)
{
}

dr::GameObject::GameObject(dr::GameObject* parent):
m_parent(parent)
{

}

dr::GameObject::~GameObject(void)
{

}

dr::Transform dr::GameObject::getTransform(void) const
{
	return m_transform;
}

dr::GameObject* dr::GameObject::getParent(void)
{
	return m_parent;
}

std::vector<dr::GameObject*> dr::GameObject::getChildren(void)
{
	return m_children;
}

std::vector<dr::GameComponent*> dr::GameObject::getComponents(void) const
{
	return m_components;
}

int dr::GameObject::getComponentCount(void) const
{
	return m_components.size();
}

int dr::GameObject::getChildrenCount(void) const
{
	return m_children.size();
}

dr::GameObject* dr::GameObject::getChild(int index)
{
	return m_children.at(index);
}

dr::GameComponent* dr::GameObject::getComponent(int index)
{
	return m_components.at(index);
}

void dr::GameObject::setTransform(const dr::Transform& transform)
{
	m_transform = transform;
}

void dr::GameObject::setParent(dr::GameObject* parent)
{
	m_parent = parent;
}

void dr::GameObject::update(float delta)
{
	for (std::vector<dr::GameComponent*>::iterator itr = m_components.begin(); itr != m_components.end(); itr++)
		(*itr)->update(delta);
}

void dr::GameObject::render(void)
{
	for (std::vector<dr::GameComponent*>::iterator itr = m_components.begin(); itr != m_components.end(); itr++)
		(*itr)->render(m_transform);
}

void dr::GameObject::updateAll(float delta)
{
	update(delta);

	for (std::vector<dr::GameObject*>::iterator itr = m_children.begin(); itr != m_children.end(); itr++)
		(*itr)->updateAll(delta);
}

void dr::GameObject::renderAll(void)
{
	render();
	for (std::vector<dr::GameObject*>::iterator itr = m_children.begin(); itr != m_children.end(); itr++)
		(*itr)->renderAll();
}

void dr::GameObject::attachComponent(dr::GameComponent* component)
{
	m_components.push_back(component);
}

void dr::GameObject::detachComponent(int componentIndex)
{
	int i = 0;
	std::vector<dr::GameComponent*>::iterator itr = m_components.begin();
	while (i++ < componentIndex)
		itr++;
	m_components.erase(itr);
}

void dr::GameObject::attachChild(dr::GameObject* child)
{
	m_children.push_back(child);
}

void dr::GameObject::detachChild(int childIndex)
{
	int i = 0;
	std::vector<dr::GameObject*>::iterator itr = m_children.begin();
	while (i++ < childIndex)
		itr++;
	m_children.erase(itr);
}