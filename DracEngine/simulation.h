#ifndef SIMULATION_H
#define SIMULATION_H

namespace dr
{

	const float THREAD_SLEEP_MASK = 0.5f;

	class SimulationManager
	{
	private:

		void(*m_updateCallback)(float);
		void(*m_renderCallback)(void);

		unsigned int m_ups;
		unsigned int m_fps;
		unsigned int m_upsCap;
		unsigned int m_fpsCap;
		bool m_running;
		
		void run(void);
		void update(float delta);
		void render(void);

	public:

		SimulationManager(void(*updateCallback)(float), void(*renderCallback)(void), unsigned int maxUps, unsigned int maxFps);
		~SimulationManager(void);

		unsigned int getUps(void) const;
		unsigned int getFps(void) const;
		unsigned int getUpsCap(void) const;
		unsigned int getFpsCap(void) const;
		bool isRunning(void) const;

		void setUpsCap(unsigned int maxUps);
		void setFpsCap(unsigned int maxFps);

		void beginSimulation(void);
		void endSimulation(void);
	};
}

#endif