#include "scriptFunction.h"

dr::ScriptFunction::ScriptFunction(void)
{
	m_byteCodeLength = 0;
	m_byteCode = nullptr;
}

dr::ScriptFunction::ScriptFunction(int byteCodeLength, unsigned int* byteCode)
{
	m_byteCodeLength = byteCodeLength;
	m_byteCode = byteCode;
}

dr::ScriptFunction::~ScriptFunction(void)
{
	delete m_byteCode;
}

int dr::ScriptFunction::getByteCodeLength(void) const
{
	return m_byteCodeLength;
}

unsigned int* dr::ScriptFunction::getByteCode(void)
{
	return m_byteCode;
}

void dr::ScriptFunction::setByteCodeLength(int byteCodeLength)
{
	m_byteCodeLength = byteCodeLength;
}

void dr::ScriptFunction::setByteCode(unsigned int* byteCode)
{
	m_byteCode = byteCode;
}