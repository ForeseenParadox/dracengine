#ifndef VEC_3_H
#define VEC_3_H

namespace dr
{
	class Vec3f
	{
	private:
		float m_x;
		float m_y;
		float m_z;
	public:

		Vec3f(void);
		Vec3f(float x, float y, float z);
		Vec3f(const dr::Vec3f& other);
		Vec3f(dr::Vec3f&& other);
		~Vec3f(void);

		float getX(void) const;
		float getY(void) const;
		float getZ(void) const;

		void setX(float x);
		void setY(float y);
		void setZ(float z);

		float length(void) const;
		float distance(const dr::Vec3f& other) const;
		float dot(const dr::Vec3f& other) const;
		dr::Vec3f cross(const dr::Vec3f& other);

		void initZero(void);

		Vec3f* mutateAdd(const dr::Vec3f& other);
		Vec3f* mutateAdd(float x, float y, float z);
		Vec3f* mutateSubtract(const dr::Vec3f& other);
		Vec3f* mutateSubtract(float x, float y, float z);
		Vec3f* mutateScale(float scaler);
		Vec3f* mutateScale(float scaleX, float scaleY, float scaleZ);
		Vec3f* mutateScale(const dr::Vec3f& other);
		Vec3f* mutateNormalize(void);

		Vec3f add(const dr::Vec3f& other) const;
		Vec3f add(float x, float y, float z) const;
		Vec3f subtract(const dr::Vec3f& other) const;
		Vec3f subtract(float x, float y, float z) const;
		Vec3f scale(float scaler) const;
		Vec3f scale(float scaleX, float scaleY, float scaleZ) const;
		Vec3f scale(const dr::Vec3f& other) const;
		Vec3f normalize(void);

		Vec3f operator+(const Vec3f& other);
		Vec3f operator-(const Vec3f& other);
		Vec3f operator*(const Vec3f& other);
		Vec3f operator*(float scaler);

		Vec3f operator+=(const Vec3f& other);
		Vec3f operator-=(const Vec3f& other);
		Vec3f operator*=(const Vec3f& other);
		Vec3f operator*=(float scaler);

		bool operator==(const dr::Vec3f& other) const;

	};
}

#endif
