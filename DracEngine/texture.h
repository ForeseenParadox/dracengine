#ifndef TEXTURE_H
#define TEXTURE_H

#include "GL\glew.h"
#include "textureResource.h"

namespace dr
{
	class Texture
	{
	private:
		GLuint m_handle;
		int m_width;
		int m_height;
		bool m_isLoaded;

		dr::Texture& operator=(const dr::Texture& other);
		Texture(const dr::Texture& other);
	public:
		Texture(void);
		Texture(const dr::TextureResource& resource);
		~Texture(void);

		GLuint getHandle(void) const;
		int getWidth(void) const;
		int getHeight(void) const;
		bool isLoaded(void) const;

		void bind(void) const;
		void loadEmptyTexture(int width, int height, GLenum filter);
		void load(const dr::TextureResource& resource);
		void load(const dr::TextureResource& resource, GLenum filter);
		void disposeTexture(void);		
	};
}

#endif