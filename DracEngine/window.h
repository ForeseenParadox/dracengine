#ifndef WINDOW_H
#define WINDOW_H

#include "subsystem.h"
#include "SDL\SDL_video.h"
#include "windowInfo.h"

namespace dr
{

	class Window : public dr::Subsystem
	{

	private:
		SDL_Window* m_window;
		SDL_GLContext m_context;

		dr::WindowInfo m_defaultWindowInfo;
	public:
		Window(const dr::EngineConfig& config);
		~Window(void);

		int getWidth(void) const;
		int getHeight(void) const;

		void setRelativeMouseModeEnabled(bool enabled);
		void setMousePosition(int x, int y);

		void init(void);
		void preRender(void);
		void render(void);
		void dispose(void);
	};
}

#endif