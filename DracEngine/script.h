#ifndef SCRIPT_H
#define SCRIPT_H

#include "scriptResource.h"
#include "scriptFunction.h"
#include <vector>
#include <string>

namespace dr
{

	class Script
	{
	private:
		int m_moduleCount;
		dr::ScriptResource* m_scripts;
	public:
		Script(dr::ScriptResource* scripts, int moduleCount);
		~Script(void);
		
		int getModuleCount(void) const;
		dr::ScriptResource* getScripts(void) const;

		void compileScript(void);
		void compileScriptFunction(std::vector<std::string>& srcCode, dr::ScriptFunction& dst);
		unsigned int* compileExpression(std::string& expression);
	};
}

#endif