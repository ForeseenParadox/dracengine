#ifndef BUFFER_USAGE_H
#define BUFFER_USAGE_H

namespace dr
{
	enum BufferUsage : unsigned int
	{
		STATIC = 0, DYNAMIC = 1, STREAM = 2
	};
}

#endif