#include "textureRegion.h"

#include <iostream>

dr::TextureRegion::TextureRegion(void):
m_texture(nullptr),
m_x(0),
m_y(0),
m_width(0),
m_height(0)
{
}

dr::TextureRegion::TextureRegion(const dr::Texture* texture):
m_texture(texture),
m_x(0),
m_y(0),
m_width(1),
m_height(1)
{
}

dr::TextureRegion::TextureRegion(const dr::Texture* texture, float x, float y, float w, float h):
m_texture(texture),
m_x(x),
m_y(y),
m_width(w),
m_height(h)
{
}

dr::TextureRegion::TextureRegion(const dr::Texture* texture, int x, int y, int w, int h) :
m_texture(texture),
m_x(0),
m_y(0),
m_width(0),
m_height(0)
{
	setX(x);
	setY(y);
	setWidth(w);
	setHeight(h);	
}

dr::TextureRegion::TextureRegion(const dr::TextureRegion& other) :
m_texture(other.m_texture),
m_x(other.m_x),
m_y(other.m_y),
m_width(other.m_width),
m_height(other.m_height)
{
}

dr::TextureRegion::~TextureRegion(void)
{
}

void dr::TextureRegion::nullTextureCrash() const
{
	std::cerr << "Can not access or set unnormalized data of a texture region with a null parent texture.";
	std::abort();
}

const dr::Texture* dr::TextureRegion::getTexture(void) const
{
	return m_texture;
}

float dr::TextureRegion::getNormalizedX(void) const
{
	return m_x;
}

float dr::TextureRegion::getNormalizedY(void) const
{
	return m_y;
}

float dr::TextureRegion::getNormalizedWidth(void) const
{
	return m_width;
}

float dr::TextureRegion::getNormalizedHeight(void) const
{
	return m_height;
}

int dr::TextureRegion::getX(void) const
{
	if (m_texture == nullptr)
		nullTextureCrash();
	return static_cast<int>(m_x * m_texture->getWidth());
}

int dr::TextureRegion::getY(void) const
{
	if (m_texture == nullptr)
		nullTextureCrash();
	return static_cast<int>(m_y * m_texture->getHeight());
}

int dr::TextureRegion::getWidth(void) const
{
	if (m_texture == nullptr)
		nullTextureCrash();
	return static_cast<int>(m_width * m_texture->getWidth());
}

int dr::TextureRegion::getHeight(void) const
{
	if (m_texture == nullptr)
		nullTextureCrash();
	return static_cast<int>(m_height * m_texture->getHeight());
}

void dr::TextureRegion::setTexture(const dr::Texture* texture)
{
	m_texture = texture;
}

void dr::TextureRegion::setNormalizedX(float x)
{
	m_x = x;
}

void dr::TextureRegion::setNormalizedY(float y)
{
	m_y = y;
}

void dr::TextureRegion::setNormalizedWidth(float width)
{
	m_width = width;
}

void dr::TextureRegion::setNormalizedHeight(float height)
{
	m_height = height;
}

void dr::TextureRegion::setX(int x)
{
	if (m_texture == nullptr)
		nullTextureCrash();
	m_x = x / static_cast<float>(m_texture->getWidth());
}

void dr::TextureRegion::setY(int y)
{
	if (m_texture == nullptr)
		nullTextureCrash();
	m_y = y / static_cast<float>(m_texture->getHeight());
}

void dr::TextureRegion::setWidth(int width)
{
	if (m_texture == nullptr)
		nullTextureCrash();
	m_width = width / static_cast<float>(m_texture->getWidth());
}

void dr::TextureRegion::setHeight(int height)
{
	if (m_texture == nullptr)
		nullTextureCrash();
	m_height = height / static_cast<float>(m_texture->getHeight());
}

dr::TextureRegion& dr::TextureRegion::operator=(const dr::TextureRegion& other)
{
	m_texture = other.m_texture;
	m_x = other.m_x;
	m_y = other.m_y;
	m_width = other.m_width;
	m_height = other.m_height;
	
	return *this;
}