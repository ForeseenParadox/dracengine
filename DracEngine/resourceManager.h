#ifndef RESOURCE_MANAGER_H
#define RESOURCE_MANAGER_H

#include <string>
#include <unordered_map>
#include "resource.h"
#include "subsystem.h"
#include "econfig.h"

namespace dr
{
	class ResourceManager : public dr::Subsystem
	{
	private:
		std::unordered_map<std::string, Resource*> m_resourceMap;

		bool m_loading;
		bool m_multiThreadLoad;

		void(*m_loadCallback)(void);

		static int loadAllResourcesFunction(void* data);
		static int unloadAllResourcesFunction(void* data);
	public:
		ResourceManager(const dr::EngineConfig& config, void(*loadCallback)(void));
		~ResourceManager(void);

		std::unordered_map<std::string, Resource*> getResourceMap(void) const;
		bool isLoading(void) const;
		bool isMultiThreadLoad(void) const;

		void setMultiThreadLoad(bool multiThreadLoad);

		void init(void);
		void dispose(void);

		bool insertResource(const std::string& name, Resource* resource);
		void removeResource(const std::string& name);

		void loadAllResources(void);
		void unloadAllResources(void);

		Resource* getResource(const std::string& name);
	};
}

#endif