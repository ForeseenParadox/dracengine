#ifndef TEXTURE_RESOURCE_H
#define TEXTURE_RESOURCE_H

#include "singleResource.h"

namespace dr
{
	class TextureResource : public dr::SingleResource
	{
	private:
		unsigned char* m_pixelData;
		unsigned int m_width;
		unsigned int m_height;
	public:
		TextureResource(const std::string& path);
		~TextureResource(void);

		unsigned char* getPixelData(void) const;
		unsigned int getWidth(void) const;
		unsigned int getHeight(void) const;
		
		void load(void);
		void unload(void);
	};
}

#endif