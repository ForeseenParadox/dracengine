#include "mat4.h"
#include <iostream>
#include <math.h>

dr::Matrix4f::Matrix4f(void)
{
	m_data = new float[16];
	initZero();
}

dr::Matrix4f::Matrix4f(const dr::Matrix4f& copy)
{
	m_data = new float[16];
	// deep copy
	float* copyData = copy.getData();
	for (int i = 0; i < 16; i++)
		m_data[i] = copyData[i];
}

dr::Matrix4f::Matrix4f(dr::Matrix4f&& rv)
{
	// move
	m_data = rv.getData();
	rv.m_data = nullptr;
}

dr::Matrix4f::~Matrix4f(void)
{
	delete m_data;
}

float* dr::Matrix4f::getData(void) const
{
	return m_data;
}

float dr::Matrix4f::getElement(int r, int c) const
{
	return m_data[c + r * 4];
}

void dr::Matrix4f::setElement(int r, int c, float v)
{
	m_data[c + r * 4] = v;
}

dr::Matrix4f dr::Matrix4f::add(const dr::Matrix4f& other) const
{
	dr::Matrix4f&& temp = Matrix4f();
	for (int i = 0; i < 16; i++)
		temp.m_data[i] = m_data[i] + other.m_data[i];
	return std::move(temp);
}

dr::Matrix4f dr::Matrix4f::subtract(const dr::Matrix4f& other) const
{
	dr::Matrix4f&& temp = Matrix4f();
	for (int i = 0; i < 16; i++)
		temp.m_data[i] = m_data[i] - other.m_data[i];
	return std::move(temp);
}

dr::Matrix4f dr::Matrix4f::multiply(const dr::Matrix4f& other) const
{
	dr::Matrix4f&& temp = Matrix4f();
	for (int r = 0; r < 4; r++)
	{
		for (int c = 0; c < 4; c++)
		{
			// progressively calculate the dot product
			// this method doesn't consume much memory
			for (int i = 0; i < 4; i++)
				temp.m_data[c + r * 4] += m_data[c + i * 4] * other.m_data[i + r * 4];		
		}
	}
	return std::move(temp);
}

dr::Matrix4f dr::Matrix4f::scale(float scaler) const
{
	dr::Matrix4f&& temp = Matrix4f();
	for (int i = 0; i < 16; i++)
		temp.m_data[i] = m_data[i] * scaler;
	return std::move(temp);
}

dr::Matrix4f* dr::Matrix4f::mutateAdd(const dr::Matrix4f& other)
{
	for (int i = 0; i < 16; i++)
		m_data[i] += other.m_data[i];
	return this;
}

dr::Matrix4f* dr::Matrix4f::mutateSubtract(const dr::Matrix4f& other)
{
	for (int i = 0; i < 16; i++)
		m_data[i] -= other.m_data[i];
	return this;
}

dr::Matrix4f* dr::Matrix4f::mutateMultiply(const dr::Matrix4f& other)
{
	float* newData = new float[16];
	for (int r = 0; r < 4; r++)
	{
		for (int c = 0; c < 4; c++)
		{
			// progressively calculate the dot product
			// this method doesn't consume much memory
			for (int i = 0; i < 4; i++)
				newData[c + r * 4] += m_data[c + i * 4] * other.m_data[i + r * 4];
		}
	}
	m_data = newData;
	return this;
}

dr::Matrix4f* dr::Matrix4f::mutateMultiply(float* x, float* y)
{

	float newX = m_data[0] * *x + m_data[1] * *y + m_data[2] * 0 + m_data[3] * 1;
	float newY = m_data[4] * *x + m_data[5] * *y + m_data[6] * 0 + m_data[7] * 1;

	*x = newX;
	*y = newY;

	return this;
}

dr::Matrix4f* dr::Matrix4f::mutateMultiply(float* x, float* y, float* z)
{

	float newX = m_data[0] * *x + m_data[1] * *y + m_data[2] * *z + m_data[3] * 1;
	float newY = m_data[4] * *x + m_data[5] * *y + m_data[6] * *z + m_data[7] * 1;
	float newZ = m_data[8] * *x + m_data[7] * *y + m_data[6] * *z + m_data[11] * 1;

	*x = newX;
	*y = newY;
	*z = newZ;

	return this;
}

dr::Matrix4f* dr::Matrix4f::mutateMultiply(float* x, float* y, float* z, float* w)
{
	
	float newX = m_data[0] * *x + m_data[1] * *y + m_data[2] * *z + m_data[3] * *w;
	float newY = m_data[4] * *x + m_data[5] * *y + m_data[6] * *z + m_data[7] * *w;
	float newZ = m_data[8] * *x + m_data[7] * *y + m_data[6] * *z + m_data[11] * *w;
	float newW = m_data[12] * *x + m_data[13] * *y + m_data[14] * *z + m_data[15] * *w;

	*x = newX;
	*y = newY;
	*z = newZ;
	*w = newW;

	return this;
}

dr::Matrix4f* dr::Matrix4f::mutateMultiply(dr::Vec2f& other)
{

	float newX = m_data[0] * other.getX() + m_data[1] * other.getY() + m_data[2] * 0 + m_data[3] * 1;
	float newY = m_data[4] * other.getX() + m_data[5] * other.getY() + m_data[6] * 0 + m_data[7] * 1;

	other.setX(newX);
	other.setY(newY);

	return this;
}

dr::Matrix4f* dr::Matrix4f::mutateScale(float scaler)
{
	for (int i = 0; i < 16; i++)
		m_data[i] *= scaler;
	return this;
}

dr::Matrix4f& dr::Matrix4f::initIdentity(void)
{
	for (int r = 0; r < 4; r++)
	{
		for (int c = 0; c < 4; c++)
		{
			if (r == c)
				m_data[c + r * 4] = 1;
			else
				m_data[c + r * 4] = 0;
		}
	}
	return *this;
}

dr::Matrix4f& dr::Matrix4f::initZero(void)
{
	for (int r = 0; r < 4; r++)
	{
		for (int c = 0; c < 4; c++)
		{
			m_data[c + r * 4] = 0;
		}
	}
	return *this;
}

dr::Matrix4f& dr::Matrix4f::initOrthographicProjection(float left, float right, float bottom, float top, float zNear, float zFar)
{
	initIdentity();

	m_data[0] = 2 / (right - left);
	m_data[3] = -(right + left) / (right-left);
	m_data[5] = 2 / (top - bottom);
	m_data[7] = -(top + bottom) / (top - bottom);
	m_data[10] = -2 / (zFar - zNear);
	m_data[11] = -(zFar + zNear) / (zFar - zNear);

	return *this;
}

dr::Matrix4f& dr::Matrix4f::initPerspectiveProjection(float fov, float aspect, float zNear, float zFar)
{
	initZero();

	m_data[0] = 1.0f / (aspect * tan(fov / 2));
	m_data[5] = 1.0f / tan(fov / 2);
	m_data[10] = (-zNear - zFar) / (zNear - zFar);
	m_data[11] = (2 * zNear * zFar) / (zNear - zFar);
	m_data[14] = 1;

	return *this;
}


dr::Matrix4f& dr::Matrix4f::initTranslation(float x, float y)
{
	initIdentity();
	m_data[3] = x;
	m_data[7] = y;
	return *this;
}

dr::Matrix4f& dr::Matrix4f::initTranslation(float x, float y, float z)
{
	initIdentity();
	m_data[3] = x;
	m_data[7] = y;
	m_data[11] = z;
	return *this;
}

dr::Matrix4f& dr::Matrix4f::initRotationX(float theta)
{
	initIdentity();
	m_data[5] = cos(theta);
	m_data[6] = -sin(theta);
	m_data[9] = sin(theta);
	m_data[10] = cos(theta);
	return *this;
}

dr::Matrix4f& dr::Matrix4f::initRotationY(float theta)
{
	initIdentity();
	m_data[0] = cos(theta);
	m_data[2] = -sin(theta);
	m_data[8] = sin(theta);
	m_data[10] = cos(theta);
	return *this;
}

dr::Matrix4f& dr::Matrix4f::initRotationZ(float theta)
{
	initIdentity();
	m_data[0] = cos(theta);
	m_data[1] = -sin(theta);
	m_data[4] = sin(theta);
	m_data[5] = cos(theta);
	return *this;
}

dr::Matrix4f& dr::Matrix4f::initScale(float sx, float sy)
{
	initIdentity();
	m_data[0] = sx;
	m_data[5] = sy;
	return *this;
}

dr::Matrix4f& dr::Matrix4f::initScale(float sx, float sy, float sz)
{
	initIdentity();
	m_data[0] = sx;
	m_data[5] = sy;
	m_data[10] = sz;

	return *this;
}

const float dr::Matrix4f::operator[](int index) const
{	
	return m_data[index];
}

dr::Matrix4f dr::Matrix4f::operator+(const dr::Matrix4f& other) const
{
	return std::move(add(other));
}

dr::Matrix4f dr::Matrix4f::operator-(const dr::Matrix4f& other) const
{
	return std::move(subtract(other));
}

dr::Matrix4f dr::Matrix4f::operator*(const dr::Matrix4f& other) const
{
	return std::move(multiply(other));
}

dr::Matrix4f dr::Matrix4f::operator*(float scaler) const
{
	return std::move(scale(scaler));
}

dr::Matrix4f dr::Matrix4f::operator=(const dr::Matrix4f& other)
{
	for (int i = 0; i < 16; i++)
		m_data[i] = other.m_data[i];
	return *this;
}
