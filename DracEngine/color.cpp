#include "color.h"
#include <iostream>

dr::Color::Color():
m_r(1.0f),
m_g(1.0f),
m_b(1.0f),
m_a(1.0f)
{
}

dr::Color::Color(float r, float g, float b, float a) :
m_r(r),
m_g(g),
m_b(b),
m_a(a)
{
}

dr::Color::Color(float r, float g, float b) :
m_r(r),
m_g(g),
m_b(b),
m_a(1.0f)
{
}

dr::Color::Color(int r, int g, int b, int a)
{
	setRed255(r);
	setGreen255(g);
	setBlue255(b);
	setAlpha255(255);
}

dr::Color::Color(int r, int g, int b)
{
	setRed255(r);
	setGreen255(g);
	setBlue255(b);
	setAlpha255(255);
}

dr::Color::Color(const dr::Color& other) :
m_r(other.m_r),
m_g(other.m_g),
m_b(other.m_b),
m_a(other.m_a)
{
}

float dr::Color::getRed(void) const
{
	return m_a;
}

float dr::Color::getGreen(void) const
{
	return m_g;
}

float dr::Color::getBlue(void) const
{
	return m_b;
}

float dr::Color::getAlpha(void) const
{
	return m_a;
}

int dr::Color::getRed255(void) const
{
	return static_cast<int>(m_r * 255);
}

int dr::Color::getGreen255(void) const
{
	return static_cast<int>(m_g * 255);
}

int dr::Color::getBlue255(void) const
{
	return static_cast<int>(m_b * 255);
}

int dr::Color::getAlpha255(void) const
{
	return static_cast<int>(m_a * 255);
}

void dr::Color::setRed(float r)
{
	m_r = r;
}

void dr::Color::setGreen(float g)
{
	m_g = g;
}

void dr::Color::setBlue(float b)
{
	m_b = b;
}

void dr::Color::setAlpha(float a)
{
	m_a = a;
}

void dr::Color::setRed255(int r)
{
	m_r = r / 255.0f;
}

void dr::Color::setGreen255(int g)
{
	m_g = g / 255.0f;
}

void dr::Color::setBlue255(int b)
{
	m_b = b / 255.0f;
}

void dr::Color::setAlpha255(int a)
{
	m_a = a / 255.0f;
}

bool dr::Color::operator==(const dr::Color& other)
{
	if (m_r == other.m_r && m_g == other.m_g && m_b == other.m_b && m_a == other.m_a)
		return true;
	else
		return false;
}

dr::Color& dr::Color::operator=(const dr::Color& other)
{
	m_a = other.m_a;
	m_r = other.m_r;
	m_g = other.m_g;
	m_b = other.m_b;

	return *this;
}