#ifndef SPRITE_BATCH_H
#define SPRITE_BATCH_H

#include "batch.h"
#include "color.h"
#include "textureRegion.h"
#include "mat4.h"
#include "vertexBuffer.h"
#include "vertexArray.h"
#include "shader.h"

namespace dr
{

	class Texture;

	class SpriteBatch : public dr::Batch
	{

		static const int ATTRIB_COUNT = 3;
		static const dr::VertexAttrib ATTRIBS[ATTRIB_COUNT];
		const int DEFAULT_MAX_DRAWS = 10000;

	private:
		// buffers
		dr::VertexBuffer m_vbo;
		dr::VertexArray m_vao;

		// rendering info
		const dr::Texture* m_currentTexture;
		dr::Color m_tint;
		int m_maxDraws;
		int m_draws;
		bool m_began;

		// shader information
		dr::ShaderProgram m_shader;
		const dr::ShaderProgram* m_customShader;
		dr::Matrix4f m_viewProjection;

		// utils
		dr::Matrix4f m_rotationBuffer;

		void initSpriteBatch();
	public:
		// Creates a new sprite batch with a default amount of max draw calls.
		SpriteBatch(void);
		// Creates a new sprite batch with the specified amount of max draw calls.
		// @param maxDraws The maximum amount of sprites that the current sprite batcher will attempt to render before batching.
		SpriteBatch(int maxDraws);
		// Creates a new sprite batch with the custom shader and a default amount of max draw calls.
		// @param customShader A custom shader to use when rendering sprites.
		SpriteBatch(dr::ShaderProgram* customShader);
		// Creates a new sprite batch with the custom shader and a default amount of max draw calls.
		// @param customShader A custom shader to use when rendering sprites.
		// @param maxDraws The maximum amount of sprites that the current sprite batcher will attempt to render before batching.
		SpriteBatch(dr::ShaderProgram* customShader, int maxDraws);
		// Creates a new sprite batch based off of an existing sprite batch.
		// @param other The sprite batch to copy into this existing sprite batch.
		SpriteBatch(const dr::SpriteBatch& other);
		// Disposes of this sprite batch.
		~SpriteBatch(void);

		// @return Whether the current sprite batch is in rendering mode.
		bool hasBegan(void) const;
		// @return The maximum amount of sprites that the current sprite batcher will attempt to render before batching.
		int getMaxDraws(void) const;
		// @return The current amount of sprites that are scheduled to be rendered during the next batch call.
		int getCurrentDraws(void) const;
		// @return A pointer to the OpenGL texture object that the sprite batch is currently working with.
		const dr::Texture* getCurrentTexture(void) const;
		// @return A reference to a color object used to tint rendered sprites.
		dr::Color& getTint(void);
		// @return The default shader used by this sprite batch.
		const dr::ShaderProgram& getShader(void) const;
		// @return The current custom shader used by this sprite batch, or null for none.
		const dr::ShaderProgram* getCustomShader(void) const;
		// @return A reference to the matrix used to define the translation and projection of the screen.
		dr::Matrix4f& getViewProjectionMatrix(void);
		
		// Sets the color to tint rendered sprites.
		// @param A constant reference to the new sprite tinting color.
		void setTint(const dr::Color& tint);
		// Sets the custom shader used by this sprite batch.
		// @param customShader The custom shader to use for this sprite batch.
		void setCustomShader(const dr::ShaderProgram* customShader);

		// Renders a section of a texture defined by the specified texture region to the screen.
		// @param x The x coordinate to render the texture to the screen
		// @param y The y coordinate to render the texture to the screen
		void render(const dr::TextureRegion& texture, float x, float y);
		// Renders a section of a texture defined by the specified texture region to the screen.
		// @param x The x coordinate to render the texture to the screen
		// @param y The y coordinate to render the texture to the screen
		// @param originX The translation on the x axis, relative to the bottom left of the sprite, when performing rotations and scalings.
		// @param originY The translation on the y axis, relative to the bottom left of the sprite, when performing rotations and scalings.
		// @param rotation The rotation in radians to rotate the sprite.
		void render(const dr::TextureRegion& texture, float x, float y, float originX, float originY, float rotation);
		// Renders a section of a texture defined by the specified texture region to the screen.
		// @param x The x coordinate to render the texture to the screen
		// @param y The y coordinate to render the texture to the screen
		// @param originX The translation on the x axis, relative to the bottom left of the sprite, when performing rotations and scalings.
		// @param originY The translation on the y axis, relative to the bottom left of the sprite, when performing rotations and scalings.
		// @param rotation The rotation in radians to rotate the sprite.
		// @param scaleX The magnitude to scale the sprite on the x axis.
		// @param scaleY The magnitude to scale the sprite on the y axis.
		void render(const dr::TextureRegion& texture, float x, float y, float originX, float originY, float rotation, float scaleX, float scaleY);
		// Renders the specified texture to the screen.
		// @param x The x coordinate to render the texture to the screen
		// @param y The y coordinate to render the texture to the screen
		void render(const dr::Texture* texture, float x, float y);
		// Renders the specified texture to the screen.
		// @param x The x coordinate to render the texture to the screen
		// @param y The y coordinate to render the texture to the screen
		// @param originX The translation on the x axis, relative to the bottom left of the sprite, when performing rotations and scalings.
		// @param originY The translation on the y axis, relative to the bottom left of the sprite, when performing rotations and scalings.
		// @param rotation The rotation in radians to rotate the sprite.
		void render(const dr::Texture* texture, float x, float y, float originX, float originY, float rotation);
		// Renders the specified texture to the screen.
		// @param x The x coordinate to render the texture to the screen
		// @param y The y coordinate to render the texture to the screen
		// @param originX The translation on the x axis, relative to the bottom left of the sprite, when performing rotations and scalings.
		// @param originY The translation on the y axis, relative to the bottom left of the sprite, when performing rotations and scalings.
		// @param rotation The rotation in radians to rotate the sprite.
		// @param scaleX The magnitude to scale the sprite on the x axis.
		// @param scaleY The magnitude to scale the sprite on the y axis.
		void render(const dr::Texture* texture, float x, float y, float originX, float originY, float rotation, float scaleX, float scaleY);
		// Renders the specified texture to the screen.
		// @param x The x coordinate to render the texture to the screen
		// @param y The y coordinate to render the texture to the screen
		// @param tx The normalized x coordinate region to render of this sprite.
		// @param ty The normalized y coordinate region to render of this sprite.
		// @param tw The normalized width of the region to render of this sprite.
		// @param th The normalized height of the region to render of this sprite.
		void render(const dr::Texture* texture, float x, float y, float tx, float ty, float tw, float th);
		// Renders the specified texture to the screen.
		// @param x The x coordinate to render the texture to the screen
		// @param y The y coordinate to render the texture to the screen
		// @param tx The normalized x coordinate region to render of this sprite.
		// @param ty The normalized y coordinate region to render of this sprite.
		// @param tw The normalized width of the region to render of this sprite.
		// @param th The normalized height of the region to render of this sprite.
		// @param originX The translation on the x axis, relative to the bottom left of the sprite, when performing rotations and scalings.
		// @param originY The translation on the y axis, relative to the bottom left of the sprite, when performing rotations and scalings.
		// @param rotation The rotation in radians to rotate the sprite.
		void render(const dr::Texture* texture, float x, float y, float tx, float ty, float tw, float th, float originX, float originY, float rotation);
		// Renders the specified texture to the screen.
		// @param x The x coordinate to render the texture to the screen
		// @param y The y coordinate to render the texture to the screen
		// @param tx The normalized x coordinate region to render of this sprite.
		// @param ty The normalized y coordinate region to render of this sprite.
		// @param tw The normalized width of the region to render of this sprite.
		// @param th The normalized height of the region to render of this sprite.
		// @param originX The translation on the x axis, relative to the bottom left of the sprite, when performing rotations and scalings.
		// @param originY The translation on the y axis, relative to the bottom left of the sprite, when performing rotations and scalings.
		// @param rotation The rotation in radians to rotate the sprite.
		// @param scaleX The magnitude to scale the sprite on the x axis.
		// @param scaleY The magnitude to scale the sprite on the y axis.
		void render(const dr::Texture* texture, float x, float y, float tx, float ty, float tw, float th, float originX, float originY, float rotation, float scaleX, float scaleY);
		
		// Begins rendering mode. This function must be invoked before any rendering is completed with the sprite batch.
		void begin(void);
		// Sends all current data stored to the GPU to render.
		void flush(void);
		// Ends rendering mode. This function must be invoked when rendering has been complete.
		void end(void);
	};

}


#endif