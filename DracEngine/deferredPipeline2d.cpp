#include "deferredPipeline2d.h"
#include "debug.h"
#include "engine.h"
#include "window.h"

const unsigned int dr::DeferredPipeline2D::S_AMBIENT_VERTEX_ATTRIB_COUNT = 2;
const dr::VertexAttrib dr::DeferredPipeline2D::S_AMBIENT_VERTEX_ATTRIBS[S_AMBIENT_VERTEX_ATTRIB_COUNT] = { dr::VertexAttrib(0, 2, dr::POSITION_ATTRIB_NAME), dr::VertexAttrib(1, 2, dr::TEXCOORD_ATTRIB_NAME) };
const unsigned int dr::DeferredPipeline2D::S_POINT_VERTEX_ATTRIB_COUNT = 1;
const dr::VertexAttrib dr::DeferredPipeline2D::S_POINT_VERTEX_ATTRIBS[S_POINT_VERTEX_ATTRIB_COUNT] = { dr::VertexAttrib(0, 2, dr::POSITION_ATTRIB_NAME) };

dr::DeferredPipeline2D::DeferredPipeline2D(void):
m_ambientPass(GL_STATIC_DRAW, 16),
m_pointPass(GL_STATIC_DRAW, 8),
m_colorBuffer(true, false, false)
{
	initAmbientPass();
	initPointPass();
}

dr::DeferredPipeline2D::~DeferredPipeline2D(void)
{
}

dr::RenderPass::RenderPass(GLenum drawMode, int vboSize):
m_vbo(drawMode, vboSize)
{
}

void dr::DeferredPipeline2D::initAmbientPass()
{
	m_ambientPass.m_vbo.pushData(-1).pushData(-1);
	m_ambientPass.m_vbo.pushData(0).pushData(0);
	m_ambientPass.m_vbo.pushData(-1).pushData(1);
	m_ambientPass.m_vbo.pushData(0).pushData(1);
	m_ambientPass.m_vbo.pushData(1).pushData(1);
	m_ambientPass.m_vbo.pushData(1).pushData(1);
	m_ambientPass.m_vbo.pushData(1).pushData(-1);
	m_ambientPass.m_vbo.pushData(1).pushData(0);
	m_ambientPass.m_vbo.flushData();
	m_ambientPass.m_vao.bindBuffer(&m_ambientPass.m_vbo, S_AMBIENT_VERTEX_ATTRIBS, S_AMBIENT_VERTEX_ATTRIB_COUNT);

	std::string vertexShaderSource =
		std::string("#version 110\n") +
		std::string("attribute vec2 a_position;\n") +
		std::string("attribute vec2 a_texCoord;\n") +
		std::string("varying vec2 v_texCoord;\n") +
		std::string("void main()\n") +
		std::string("{\n") +
		std::string("	gl_Position = vec4(a_position, 0.0, 1.0);\n") +
		std::string("	v_texCoord = a_texCoord;\n") +
		std::string("}");

	std::string fragmentShaderSource =
		std::string("#version 110\n") +
		std::string("uniform sampler2D u_sampler;\n") +
		std::string("uniform vec3 u_ambientColor;\n") +
		std::string("uniform float u_ambientIntensity;\n") +
		std::string("varying vec2 v_texCoord;\n") +
		std::string("void main()\n") +
		std::string("{\n") +
		std::string("	gl_FragColor = texture2D(u_sampler, v_texCoord) * vec4(u_ambientColor, 1.0) * u_ambientIntensity;\n") +
		std::string("}");

	m_ambientPass.m_shader.bindAttribs(S_AMBIENT_VERTEX_ATTRIBS, S_AMBIENT_VERTEX_ATTRIB_COUNT);
	m_ambientPass.m_shader.compileAndLink(vertexShaderSource, fragmentShaderSource);
}

void dr::DeferredPipeline2D::initPointPass()
{
	m_pointPass.m_vbo.pushData(-1).pushData(-1);
	m_pointPass.m_vbo.pushData(-1).pushData(1);
	m_pointPass.m_vbo.pushData(1).pushData(1);
	m_pointPass.m_vbo.pushData(1).pushData(-1);
	m_pointPass.m_vbo.flushData();
	m_pointPass.m_vao.bindBuffer(&m_pointPass.m_vbo, S_POINT_VERTEX_ATTRIBS, S_POINT_VERTEX_ATTRIB_COUNT);

	std::string vertexShaderSource =
		std::string("#version 110\n") +
		std::string("attribute vec2 a_position;\n") +
		std::string("varying vec2 v_position;\n") +
		std::string("void main()\n") +
		std::string("{\n") +
		std::string("	gl_Position = vec4(a_position, 0.0, 1.0);\n") +
		std::string("	v_position = a_position;\n") +
		std::string("}\n") +
		std::string("");
	std::string fragmentShaderSource =
		std::string("#version 110\n") +
		std::string("uniform vec2 u_resolution;\n") +
		std::string("uniform float u_intensity;\n") +
		std::string("uniform vec3 u_color;\n") +
		std::string("uniform vec2 u_position;\n") +
		std::string("uniform vec3 u_attenuation;\n") +
		std::string("varying vec2 v_position;\n") +
		std::string("void main()\n") +
		std::string("{\n") +
		std::string("	vec2 world = v_position * u_resolution / 2 + u_resolution / 2;\n") +
		std::string("	float dist = distance(u_position, world);\n") +
		std::string("	float atten = 1.0 / (u_attenuation.x * dist * dist + u_attenuation.y * dist + u_attenuation.z);\n") +
		std::string("	gl_FragColor = vec4(u_color, 1.0) * atten;\n") +
		std::string("}\n") +
		std::string("");

	m_pointPass.m_shader.bindAttribs(S_POINT_VERTEX_ATTRIBS, S_POINT_VERTEX_ATTRIB_COUNT);
	m_pointPass.m_shader.compileAndLink(vertexShaderSource, fragmentShaderSource);
}

void dr::DeferredPipeline2D::enableFBO()
{
	if (m_renderPass == PASS_COLOR)
		m_colorBuffer.bind();
}

void dr::DeferredPipeline2D::disableFBO()
{
	dr::FrameBuffer::bindDefaultFrameBuffer();
}

const dr::FrameBuffer& dr::DeferredPipeline2D::getColorBuffer(void) const
{
	return m_colorBuffer;
}

unsigned int dr::DeferredPipeline2D::getRenderPass(void) const
{
	return m_renderPass;
}

dr::SpriteBatch& dr::DeferredPipeline2D::getSpriteBatch(void)
{
	return m_spriteBatch;
}

dr::AmbientLight& dr::DeferredPipeline2D::getAmbientLight(void)
{
	return m_ambientLight;
}

void dr::DeferredPipeline2D::beginPass(unsigned int renderPass)
{
	m_renderPass = renderPass;

	enableFBO();
}

void dr::DeferredPipeline2D::endPass(void)
{
	m_renderPass = PASS_DISABLED;
	
	disableFBO();
}

void dr::DeferredPipeline2D::renderAmbientLightPass(void)
{
	ASSERT(m_renderPass == PASS_DISABLED, "A rendering pass is still enabled, please disable before rendering the ambient pass.")

	m_colorBuffer.getColorAttachment()->bind();
	m_ambientPass.m_shader.bindProgram();	
	m_ambientPass.m_shader.setUniformFloat(UNIFORM_AMBIENT_INTENSITY, m_ambientLight.m_base.m_intensity);
	m_ambientPass.m_shader.setUniformColor3(UNIFORM_AMBIENT_COLOR, m_ambientLight.m_base.m_color);

	// render the texture to the screen
	m_ambientPass.m_vao.render(GL_QUADS, 4);
}

void dr::DeferredPipeline2D::renderPointLightPass(const dr::PointLight& pointLight)
{
	ASSERT(m_renderPass == PASS_DISABLED, "A rendering pass is still enabled, please disable before rendering the ambient pass.")

	m_pointPass.m_shader.bindProgram();
	m_pointPass.m_shader.setUniformFloat("u_intensity", pointLight.m_base.m_intensity);
	m_pointPass.m_shader.setUniformColor3("u_color", pointLight.m_base.m_color);
	m_pointPass.m_shader.setUniformVector2("u_position", pointLight.m_position);
	m_pointPass.m_shader.setUniformVector3("u_attenuation", pointLight.m_attenuation);

	dr::Window* wnd = dr::Engine::getInstance()->getWindow();
	m_pointPass.m_shader.setUniformVector2("u_resolution", dr::Vec2f(wnd->getWidth(), wnd->getHeight()));

	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ONE);
	
	m_pointPass.m_vao.render(GL_QUADS, 4);

	glDisable(GL_BLEND);
}