#ifndef SHAPE_BATCH_H
#define SHAPE_BATCH_H

#include "batch.h"
#include "vertexAttrib.h"
#include "color.h"
#include <string>
#include "shapeBatch.h"
#include "spriteBatch.h"

namespace dr
{
	class ShapeBatch : public dr::Batch
	{
	private:
		// constants
		static const int DEFAULT_MAX_VERTEX_COUNT;
		static const int ATTRIB_COUNT;
		static VertexAttrib ATTRIBS[];

		// buffers
		dr::VertexBuffer m_vbo;
		dr::VertexArray m_vao;

		// rendering info
		dr::Color m_color;
		GLenum m_currentPrimitive;
		int m_maxVertices;
		int m_vertices;
		bool m_began;

		// shader information
		dr::ShaderProgram m_shader;
		dr::ShaderProgram* m_customShader;
		dr::Matrix4f m_viewProjection;

		// utils
		void initShapeBatch();
		void checkFlush(GLenum requestedPrimitive);
	public:
		ShapeBatch(void);
		ShapeBatch(int maxVertices);
		ShapeBatch(ShaderProgram* shader);
		ShapeBatch(ShaderProgram* shader, int maxVertices);

		GLenum getCurrentPrimitive(void) const;
		dr::Color& getColor(void);
		dr::Matrix4f& getViewProjectionMatrix(void);
		int getMaxVertices(void) const;
		int getVertices(void) const;
		bool hasBegan(void) const;
		bool isUsingCustomShader(void) const;

		void setColor(const dr::Color& color);
		void setCustomShader(dr::ShaderProgram* customShader);
		void setViewProjectionMatrix(const dr::Matrix4f& viewProjection);

		void renderLine(float x1, float y1, float x2, float y2);

		void renderRect(float x, float y, float w, float h);
		void renderRect(float x, float y, float w, float h, float ox, float oy, float rotation, float scaleX, float scaleY);

		void renderFilledRect(float x, float y, float w, float h);
		void renderFilledRect(float x, float y, float w, float h, float ox, float oy, float rotation, float scaleX, float scaleY);

		void renderCircle(float x, float y, float r);
		void renderFilledCircle(float x, float y, float r);

		void renderShape(float* xPoints, float* yPoints, int pointCount);
		void renderShape(float* pointsInterlaced, int pointCount);

		void begin(void);
		void flush(void);
		void end(void);
	};
}

#endif