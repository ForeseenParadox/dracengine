#include "vertexArray.h"
#include <iostream>

dr::VertexArray::VertexArray(void)
{
	glGenVertexArrays(1, &m_handle);
}

dr::VertexArray::~VertexArray(void)
{
	glDeleteVertexArrays(1, &m_handle);
}

GLuint dr::VertexArray::getHandle(void) const
{
	return m_handle;
}

void dr::VertexArray::bind(void) const
{
	glBindVertexArray(m_handle);
}

void dr::VertexArray::bindBuffer(const dr::VertexBuffer* vbo, const dr::VertexAttrib attribs[], int attribCount)
{

	bind();

	vbo->bind();

	// calculate stride
	unsigned int stride = 0;

	if (attribCount > 1)
		for (int i = 0; i < attribCount; i++)
			stride += attribs[i].getSize() * sizeof(float);

	for (unsigned int i = 0, offset = 0; i < attribCount; i++, offset += attribs[i - 1].getSize() * sizeof(float))
	{
		glVertexAttribPointer(attribs[i].getIndex(), attribs[i].getSize(), GL_FLOAT, GL_FALSE, stride, reinterpret_cast<const void*>(offset));
		glEnableVertexAttribArray(attribs[i].getIndex());
	}

}

void dr::VertexArray::render(GLenum primitive, dr::IndexBuffer& ibo, int vertexCount, int elementCount)
{
	bind();
	ibo.bind();
	glDrawElements(primitive, elementCount, GL_UNSIGNED_INT, (void*)(0));	
}

void dr::VertexArray::render(GLenum primitive, int elementCount)
{
	bind();
	glDrawArrays(primitive, 0, elementCount);
}

void unbindVAO(void)
{
	glBindVertexArray(0);
}