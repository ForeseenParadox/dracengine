#ifndef INPUT_H
#define INPUT_H

#include "subsystem.h"
#include "vec2.h"
#include "econfig.h"

union SDL_Event;

namespace dr
{

	class Input : public dr::Subsystem
	{
	private:

		const static int KEY_COUNT = 0xFF;
		const static int BUTTON_COUNT = 0xFF;

		int m_keysPressed[KEY_COUNT];
		int m_buttonsPressed[BUTTON_COUNT];

		int m_mouseX;
		int m_mouseY;

		int m_lastMouseX = -1;
		int m_lastMouseY = -1;
	public:
		Input(const dr::EngineConfig& config);
		~Input(void);

		void init(void);
		void dispose(void);

		bool isKeyPressed(int keyCode);
		bool isButtonPressed(int buttonCode);

		int getMouseX(void);
		int getMouseY(void);
		dr::Vec2f getRelativeMousePosition(void);
		int getDelteMouseX(void);
		int getDelteMouseY(void);

		void resetMouseDeltaX(void);
		void resetMouseDeltaY(void);

		void processEvent(SDL_Event* e);
	};
}

#endif