#ifndef VERTEX_H
#define VERTEX_H

#include "vec2.h"
#include "vec3.h"
#include "color.h"

namespace dr
{

	/* Count of the amount of elements in the posistion element of this vertex. */
	const int VERTEX_3_POSITION_SIZE = 3;
	/* Count of the amount of elements in the color element of this vertex. */
	const int VERTEX_3_COLOR_SIZE = 4;
	/* Count of the amount of elements in the texcoord element of this vertex. */
	const int VERTEX_3_TEXCOORD_SIZE = 2;
	/* Count of the amount of elements in the normal element of this vertex. */
	const int VERTEX_3_NORMAL_SIZE = 3;
	/* Sum of the count of all elements stored in this vertex.*/
	const int VERTEX_3_SIZE = VERTEX_3_POSITION_SIZE + VERTEX_3_COLOR_SIZE + VERTEX_3_TEXCOORD_SIZE + VERTEX_3_NORMAL_SIZE;

	class Vertex3
	{
	private:
		dr::Vec3f m_position;
		dr::Color m_color;
		dr::Vec2f m_texCoord;
		dr::Vec3f m_normal;
	public:

		Vertex3(void);
		Vertex3(const dr::Vec3f& position);
		Vertex3(const dr::Vec3f& position, const dr::Color& color);
		Vertex3(const dr::Vec3f& position, const dr::Color& color, const dr::Vec2f& texCoord);
		Vertex3(const dr::Vec3f& position, const dr::Color& color, const dr::Vec2f& texCoord, const dr::Vec3f& normal);
		Vertex3(const dr::Vertex3& other);
		
		dr::Vec3f getPosition(void) const;
		dr::Color getColor(void) const;
		dr::Vec2f getTexCoord(void) const;
		dr::Vec3f getNormal(void) const;

		void setPosition(const dr::Vec3f& position);
		void setColor(const dr::Color& color);
		void setTexCoord(const dr::Vec2f& texCoord);
		void setNormal(const dr::Vec3f& normal);
		
		bool operator==(dr::Vertex3& other) const;
	};
}

#endif