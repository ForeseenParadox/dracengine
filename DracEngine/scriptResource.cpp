#include "scriptResource.h"
#include <iostream>

dr::ScriptResource::ScriptResource(const std::string& path) :
dr::TextResource(path)
{

}

dr::ScriptResource::~ScriptResource(void)
{

}

void dr::ScriptResource::load(void)
{
	TextResource::load();
}

void dr::ScriptResource::unload(void)
{
	TextResource::unload();
}